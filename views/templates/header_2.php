<?php
if (!isset($_SESSION)) {
    session_start();
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="https://www.si18.com.co/img/logo.png" />
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>public/css/normalize.css" />
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>public/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>public/css/styles.css" />

    <!-- JS -->
    <script type="text/javascript" src="<?php echo constant('URL'); ?>public/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="<?php echo constant('URL'); ?>public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo constant('URL'); ?>public/js/autosize.min.js"></script>
    <script type="text/javascript" src="<?php echo constant('URL'); ?>node_modules/chart.js/dist/Chart.js"></script>
    <script type="text/javascript" src="<?php echo constant('URL'); ?>public/js/script.js"></script>
    <title>Ejercicio</title>

</head>

<body>


    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-green">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Logo Empresa</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="<?php echo constant('URL'); ?>index">Inicio</a>
                    </li>
                    <?php
                    if ((isset($_SESSION['rol_idrol'])) && (($_SESSION['rol_idrol']) == 1)) {
                    ?>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="<?php echo constant('URL'); ?>lesson">Lecciones
                                aprendidas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="<?php echo constant('URL'); ?>lesson/newlesson">Nueva leccion</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="<?php echo constant('URL'); ?>statistics">Estadisticas</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Usuarios
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="<?php echo constant('URL'); ?>user/create">Agregar
                                        usuario</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item" href="<?php echo constant('URL'); ?>user">Listado de
                                        usuarios</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="<?php echo constant('URL'); ?>login/logout">Cerrar
                                sesion</a>
                        </li>
                    <?php
                    } else {
                    ?>
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="<?php echo constant('URL'); ?>login">Iniciar
                                Sesion</a>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>

    <script>
        $(document).ready(function() {

        });
    </script>
    <br>
    <br>