<?php require 'views/templates/header.php' ?>

<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="<?php echo constant('URL'); ?>public/images/img_1.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="<?php echo constant('URL'); ?>public/images/img_2.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="<?php echo constant('URL'); ?>public/images/img_3.jpg" class="d-block w-100" alt="...">
        </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>

<br>
<hr>
<br>
<div class="container">
    <?php
    $mensaje = "";
    echo $this->mensaje;
    ?>

    <div class="row row-cols-1 row-cols-md-2 g-4">
        <div class="col">
            <div class="card text-center">
                <h5 class="card-header">¿Ya reportaste hoy?</h5>
                <img src="<?php echo constant('URL'); ?>public/images/sigo.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">Te invitamos a realizar tu reporte diario en la aplicacion SIGO TU SALUD</p>
                    <a href="http://sigotusalud.si18.com.co/" target="_blanck" class="btn btn-primary">Ir al sitio</a>
                </div>
            </div>
        </div>
        <div class="col text-center">
            <div class="card">
                <h5 class="card-header">¿Eres proveedor?</h5>
                <img src="<?php echo constant('URL'); ?>public/images/proveedores.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">Te invitamos a conocer y estar actualizado acerca de tus pagos en nuestra aplicacion de proveedores</p>
                    <a href="http://proveedores.si18.com.co/" target="_blanck" class="btn btn-primary">Ir al sitio</a>
                </div>
            </div>
        </div>
        <div class="col text-center">
            <div class="card">
                <h5 class="card-header">Apreciado colaborador</h5>
                <img src="<?php echo constant('URL'); ?>public/images/checklist.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">Recuerda siempre al recibir tu vehiculo, diligenciar el checklist de estado del bus</p>
                    <a href="http://checklist.si18.com.co/" target="_blanck" class="btn btn-primary">Ir al sitio</a>
                </div>
            </div>
        </div>
        <div class="col text-center">
            <div class="card">
                <h5 class="card-header">¿Ya conoces nuestra pagina web?</h5>
                <img src="<?php echo constant('URL'); ?>public/images/si18.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">Queremos invitarte a visitar nuestro sitio web, alli econtraras informacion actualizada acerca de nuestra familia Sí18</p>
                    <a href="https://www.si18.com.co/" target="_blanck" class="btn btn-primary">Ir al sitio</a>
                </div>
            </div>
        </div>
    </div>

</div>