<?php require 'views/templates/header_2.php' ?>
<br>
<br>
<div class="container">
    <?php
    $mensaje = "";
    echo $this->mensaje;
    ?>
    <div class="row justify-content-md-center">
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="card center-align glass">
                <div class="card-header">
                    Validar
                </div>
                <div class="card-content">
                    <br>
                    <form action="<?php echo constant('URL'); ?>login/verifyToken" method="POST">
                        <div class="container">
                            <br>
                            <label for="">Se ha enviado a su correo el token de acceso, por favor ingreselo a continuacion </label>
                            <br>
                            <br>
                            <input type="hidden" name="iduser" value="<?php echo $this->session->iduser ?>"> 
                            <div class="col-12">
                                <div class="input-group mb-3">
                                    <label for="staticEmail" class="col-12 col-md-4 col-form-label">Token</label>
                                    <div class="col-1 col-md-1">
                                        <i class="material-icons iconis prefix">account_box</i>
                                    </div>
                                    <div class="col-11 col-md-7">
                                        <input id="token" name="token" class="form-control" type="text" required aria-required="true">
                                    </div>
                                </div>
                            </div>
                            <div style="text-align: center">
                                <button class="btn btn-outline-success" type="submit" name="action">Ingresar
                                    <i class="material-icons right" style="vertical-align: middle;">send</i>
                                </button>
                            </div>
                            <br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>