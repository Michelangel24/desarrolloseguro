<?php

require 'views/templates/header.php';
?>
<div class="container-fluid">

    <div class="container-fluid border rounded" style="background-color: rgba(255,255,255,0.94);">
        <div class="container">

            <br>
            <h1>Estadisticas</h1>
            <script>

            </script>


            <br>
            <br>
            <div class="row">
                <div class="col col-lg-6" style="margin-top: 15px;">
                    <div class="card text-center">
                        <div class="card-header">
                            Lecciones aprendidas agrupadas por empresa
                        </div>
                        <div class="card-body">
                            <canvas id="myChart7" width="400" height="400"></canvas>
                            <script>
                                $(document).ready(function() {
                                    $.ajax({
                                        url: "<?php echo constant('URL'); ?>statistics/company",
                                        dataType: 'json',
                                        contentType: "application/json; charset=utf-8",
                                        method: "POST",
                                        success: function(data) {
                                            var description = [];
                                            var users = [];
                                            var color = [
                                                'rgba(255, 99, 132, 0.2)',
                                                'rgba(54, 162, 235, 0.2)',
                                                'rgba(255, 206, 86, 0.2)',
                                                'rgba(75, 192, 192, 0.2)',
                                                'rgba(153, 102, 255, 0.2)',
                                                'rgba(255, 159, 64, 0.2)',
                                                'rgba(0, 139, 139, 0.2)',
                                                'rgba(47, 79, 79, 0.2)',
                                                'rgba(0, 255, 255, 0.2)',
                                                'rgba(34, 139, 34, 0.2)',
                                                'rgba(178, 34, 34, 0.2)',
                                                'rgba(138, 43, 226, 0.2)',
                                                'rgba(222, 184, 135, 0.2)',
                                                'rgba(100, 149, 237, 0.2)',
                                                'rgba(169, 169, 169, 0.2)',
                                                'rgba(139, 0, 139, 0.2)',
                                                'rgba(255, 140, 0, 0.2)',
                                                'rgba(72, 61, 139, 0.2)',
                                                'rgba(184, 134, 11, 0.2)',
                                                'rgba(255, 20, 147, 0.2)',
                                                'rgba(233, 150, 122, 0.2)',
                                                'rgba(205, 92, 92, 0.2)',
                                                'rgba(128, 0, 0 , 0.2)',
                                                'rgba(139, 0, 0, 0.2)',
                                                'rgba(165, 42, 42, 0.2)',
                                                'rgba(220, 20, 60, 0.2)',
                                                'rgba(255, 0, 0, 0.2)',
                                                'rgba(255, 99, 71, 0.2)',
                                                'rgba(255, 127, 80, 0.2)',
                                                'rgba(240, 128, 128, 0.2)',
                                                'rgba(250, 128, 114, 0.2)',
                                                'rgba(255, 160, 122, 0.2)',
                                                'rgba(255, 69, 0, 0.2)',
                                                'rgba(255, 165, 0, 0.2)',
                                                'rgba(255, 215, 0, 0.2)',
                                                'rgba(218, 165, 32, 0.2)',
                                            ];
                                            var bordercolor = [
                                                'rgba(255, 99, 132, 1)',
                                                'rgba(54, 162, 235, 1)',
                                                'rgba(255, 206, 86, 1)',
                                                'rgba(75, 192, 192, 1)',
                                                'rgba(153, 102, 255, 1)',
                                                'rgba(255, 159, 64, 1)',
                                                'rgba(0, 139, 139, 0.1)',
                                                'rgba(47, 79, 79, 0.1)',
                                                'rgba(0, 255, 255, 0.1)',
                                                'rgba(34, 139, 34, 0.1)',
                                                'rgba(178, 34, 34, 0.1)',
                                                'rgba(138, 43, 226, 0.1)',
                                                'rgba(222, 184, 135, 0.1)',
                                                'rgba(100, 149, 237, 0.1)',
                                                'rgba(169, 169, 169, 0.1)',
                                                'rgba(139, 0, 139, 0.1)',
                                                'rgba(255, 140, 0, 0.1)',
                                                'rgba(72, 61, 139, 0.1)',
                                                'rgba(184, 134, 11, 0.1)',
                                                'rgba(255, 20, 147, 0.1)',
                                                'rgba(233, 150, 122, 0.1)',
                                                'rgba(205, 92, 92, 0.1)',
                                                'rgba(128, 0, 0 , 0.1)',
                                                'rgba(139, 0, 0, 0.1)',
                                                'rgba(165, 42, 42, 0.1)',
                                                'rgba(220, 20, 60, 0.1)',
                                                'rgba(255, 0, 0, 0.1)',
                                                'rgba(255, 99, 71, 0.1)',
                                                'rgba(255, 127, 80, 0.1)',
                                                'rgba(240, 128, 128, 0.1)',
                                                'rgba(250, 128, 114, 0.1)',
                                                'rgba(255, 160, 122, 0.1)',
                                                'rgba(255, 69, 0, 0.1)',
                                                'rgba(255, 165, 0, 0.1)',
                                                'rgba(255, 215, 0, 0.1)',
                                                'rgba(218, 165, 32, 0.1)',
                                            ];

                                            for (var i in data) {
                                                description.push(data[i].description);
                                                users.push(data[i].users);
                                            }

                                            var chartdata = {
                                                labels: description,
                                                datasets: [{
                                                    label: 'Agrupacion de usuarios registrados por empresa',
                                                    backgroundColor: color,
                                                    borderColor: color,
                                                    borderWidth: 2,
                                                    hoverBackgroundColor: color,
                                                    hoverBorderColor: bordercolor,
                                                    data: users
                                                }]
                                            };
                                            var mostrar = document.getElementById('myChart7');

                                            var grafico = new Chart(mostrar, {
                                                type: 'doughnut',
                                                data: chartdata,
                                                options: {
                                                    responsive: true,
                                                }
                                            });
                                        },
                                        error: function(data) {
                                            console.log(data);
                                        }
                                    });
                                });
                            </script>
                        </div>

                    </div>
                </div>
                <br>
                <div class="col col-lg-6" style="margin-top: 15px;">
                    <div class="card text-center">
                        <div class="card-header">
                            Lecciones aprendidas agrupadas por proceso
                        </div>

                        <div class="card-body">
                            <canvas id="myChart8" width="400" height="400"></canvas>
                            <script>
                                $(document).ready(function() {
                                    $.ajax({
                                        url: "<?php echo constant('URL'); ?>statistics/process",
                                        dataType: 'json',
                                        contentType: "application/json; charset=utf-8",
                                        method: "POST",
                                        success: function(data) {
                                            var description = [];
                                            var users = [];
                                            var totalusers = [];

                                            var color = [
                                                'rgba(255, 99, 132, 0.2)',
                                                'rgba(54, 162, 235, 0.2)',
                                                'rgba(255, 206, 86, 0.2)',
                                                'rgba(75, 192, 192, 0.2)',
                                                'rgba(153, 102, 255, 0.2)',
                                                'rgba(255, 159, 64, 0.2)',
                                                'rgba(0, 139, 139, 0.2)',
                                                'rgba(47, 79, 79, 0.2)',
                                                'rgba(0, 255, 255, 0.2)',
                                                'rgba(34, 139, 34, 0.2)',
                                                'rgba(178, 34, 34, 0.2)',
                                                'rgba(138, 43, 226, 0.2)',
                                                'rgba(222, 184, 135, 0.2)',
                                                'rgba(100, 149, 237, 0.2)',
                                                'rgba(169, 169, 169, 0.2)',
                                                'rgba(139, 0, 139, 0.2)',
                                            ];
                                            var bordercolor = [
                                                'rgba(255,99,132,1)',
                                                'rgba(54, 162, 235, 1)',
                                                'rgba(255, 206, 86, 1)',
                                                'rgba(75, 192, 192, 1)',
                                                'rgba(153, 102, 255, 1)',
                                                'rgba(255, 159, 64, 1)',
                                                'rgba(0, 139, 139, 0.1)',
                                                'rgba(47, 79, 79, 0.1)',
                                                'rgba(0, 255, 255, 0.1)',
                                                'rgba(34, 139, 34, 0.1)',
                                                'rgba(178, 34, 34, 0.1)',
                                                'rgba(138, 43, 226, 0.1)',
                                                'rgba(222, 184, 135, 0.1)',
                                                'rgba(100, 149, 237, 0.1)',
                                                'rgba(169, 169, 169, 0.1)',
                                                'rgba(139, 0, 139, 0.1)',
                                            ];

                                            for (var i in data) {
                                                description.push(data[i].description);
                                                users.push(data[i].users);
                                            }

                                            var chartdata = {
                                                labels: description,
                                                datasets: [{
                                                        label: "Procesos",
                                                        backgroundColor: color,
                                                        borderColor: color,
                                                        borderWidth: 2,
                                                        hoverBackgroundColor: color,
                                                        hoverBorderColor: bordercolor,
                                                        data: users
                                                    },
                                                ]
                                            };
                                            var mostrar = document.getElementById('myChart8');

                                            var grafico = new Chart(mostrar, {
                                                type: 'bar',
                                                data: chartdata,
                                                options: {
                                                    scales: {
                                                        yAxes: [{
                                                            ticks: {
                                                                beginAtZero: true
                                                            }
                                                        }]
                                                    },
                                                    responsive: true,
                                                }
                                            });
                                        },
                                        error: function(data) {
                                            console.log(data);
                                        }
                                    });
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col col-lg-6" style="margin-top: 15px;">
                    <div class="card text-center">
                        <div class="card-header">
                            Lecciones aprendidas agrupadas por privilegios
                        </div>
                        <div class="card-body">
                            <canvas id="myChart3" width="400" height="400"></canvas>
                            <script>
                                $(document).ready(function() {
                                    $.ajax({
                                        url: "<?php echo constant('URL'); ?>statistics/lessonType",
                                        dataType: 'json',
                                        contentType: "application/json; charset=utf-8",
                                        method: "POST",
                                        success: function(data) {
                                            var description = [];
                                            var users = [];
                                            var color = [
                                                'rgba(255, 99, 132, 0.2)',
                                                'rgba(54, 162, 235, 0.2)',
                                                'rgba(255, 206, 86, 0.2)',
                                                'rgba(75, 192, 192, 0.2)',
                                                'rgba(153, 102, 255, 0.2)',
                                                'rgba(255, 159, 64, 0.2)',
                                                'rgba(0, 139, 139, 0.2)',
                                                'rgba(47, 79, 79, 0.2)',
                                                'rgba(0, 255, 255, 0.2)',
                                                'rgba(34, 139, 34, 0.2)',
                                                'rgba(178, 34, 34, 0.2)',
                                                'rgba(138, 43, 226, 0.2)',
                                                'rgba(222, 184, 135, 0.2)',
                                                'rgba(100, 149, 237, 0.2)',
                                                'rgba(169, 169, 169, 0.2)',
                                                'rgba(139, 0, 139, 0.2)',
                                                'rgba(255, 140, 0, 0.2)',
                                                'rgba(72, 61, 139, 0.2)',
                                                'rgba(184, 134, 11, 0.2)',
                                                'rgba(255, 20, 147, 0.2)',
                                                'rgba(233, 150, 122, 0.2)',
                                                'rgba(205, 92, 92, 0.2)',
                                                'rgba(128, 0, 0 , 0.2)',
                                                'rgba(139, 0, 0, 0.2)',
                                                'rgba(165, 42, 42, 0.2)',
                                                'rgba(220, 20, 60, 0.2)',
                                                'rgba(255, 0, 0, 0.2)',
                                                'rgba(255, 99, 71, 0.2)',
                                                'rgba(255, 127, 80, 0.2)',
                                                'rgba(240, 128, 128, 0.2)',
                                                'rgba(250, 128, 114, 0.2)',
                                                'rgba(255, 160, 122, 0.2)',
                                                'rgba(255, 69, 0, 0.2)',
                                                'rgba(255, 165, 0, 0.2)',
                                                'rgba(255, 215, 0, 0.2)',
                                                'rgba(218, 165, 32, 0.2)',
                                            ];
                                            var bordercolor = [
                                                'rgba(255, 99, 132, 1)',
                                                'rgba(54, 162, 235, 1)',
                                                'rgba(255, 206, 86, 1)',
                                                'rgba(75, 192, 192, 1)',
                                                'rgba(153, 102, 255, 1)',
                                                'rgba(255, 159, 64, 1)',
                                                'rgba(0, 139, 139, 0.1)',
                                                'rgba(47, 79, 79, 0.1)',
                                                'rgba(0, 255, 255, 0.1)',
                                                'rgba(34, 139, 34, 0.1)',
                                                'rgba(178, 34, 34, 0.1)',
                                                'rgba(138, 43, 226, 0.1)',
                                                'rgba(222, 184, 135, 0.1)',
                                                'rgba(100, 149, 237, 0.1)',
                                                'rgba(169, 169, 169, 0.1)',
                                                'rgba(139, 0, 139, 0.1)',
                                                'rgba(255, 140, 0, 0.1)',
                                                'rgba(72, 61, 139, 0.1)',
                                                'rgba(184, 134, 11, 0.1)',
                                                'rgba(255, 20, 147, 0.1)',
                                                'rgba(233, 150, 122, 0.1)',
                                                'rgba(205, 92, 92, 0.1)',
                                                'rgba(128, 0, 0 , 0.1)',
                                                'rgba(139, 0, 0, 0.1)',
                                                'rgba(165, 42, 42, 0.1)',
                                                'rgba(220, 20, 60, 0.1)',
                                                'rgba(255, 0, 0, 0.1)',
                                                'rgba(255, 99, 71, 0.1)',
                                                'rgba(255, 127, 80, 0.1)',
                                                'rgba(240, 128, 128, 0.1)',
                                                'rgba(250, 128, 114, 0.1)',
                                                'rgba(255, 160, 122, 0.1)',
                                                'rgba(255, 69, 0, 0.1)',
                                                'rgba(255, 165, 0, 0.1)',
                                                'rgba(255, 215, 0, 0.1)',
                                                'rgba(218, 165, 32, 0.1)',
                                            ];

                                            for (var i in data) {
                                                description.push(data[i].description);
                                                users.push(data[i].users);
                                            }

                                            var chartdata = {
                                                labels: description,
                                                datasets: [{
                                                    label: 'privilegios',
                                                    backgroundColor: color,
                                                    borderColor: color,
                                                    borderWidth: 2,
                                                    hoverBackgroundColor: color,
                                                    hoverBorderColor: bordercolor,
                                                    data: users
                                                }]
                                            };
                                            var mostrar = document.getElementById('myChart3');

                                            var grafico = new Chart(mostrar, {
                                                type: 'bar',
                                                data: chartdata,
                                                options: {
                                                    responsive: true,
                                                }
                                            });
                                        },
                                        error: function(data) {
                                            console.log(data);
                                        }
                                    });
                                });
                            </script>
                        </div>

                    </div>
                </div>
                <br>
                <div class="col col-lg-6" style="margin-top: 15px;">
                    <div class="card text-center">
                        <div class="card-header">
                            Lecciones aprendidas agrupadas por mes
                        </div>

                        <div class="card-body">
                            <canvas id="myChart4" width="400" height="400"></canvas>
                            <script>
                                $(document).ready(function() {
                                    $.ajax({
                                        url: "<?php echo constant('URL'); ?>statistics/dateInsert",
                                        dataType: 'json',
                                        contentType: "application/json; charset=utf-8",
                                        method: "POST",
                                        success: function(data) {
                                            var description = [];
                                            var users = [];
                                            var totalusers = [];

                                            var color = [
                                                'rgba(255, 99, 132, 0.2)',
                                                'rgba(54, 162, 235, 0.2)',
                                                'rgba(255, 206, 86, 0.2)',
                                                'rgba(75, 192, 192, 0.2)',
                                                'rgba(153, 102, 255, 0.2)',
                                                'rgba(255, 159, 64, 0.2)',
                                                'rgba(0, 139, 139, 0.2)',
                                                'rgba(47, 79, 79, 0.2)',
                                                'rgba(0, 255, 255, 0.2)',
                                                'rgba(34, 139, 34, 0.2)',
                                                'rgba(178, 34, 34, 0.2)',
                                                'rgba(138, 43, 226, 0.2)',
                                                'rgba(222, 184, 135, 0.2)',
                                                'rgba(100, 149, 237, 0.2)',
                                                'rgba(169, 169, 169, 0.2)',
                                                'rgba(139, 0, 139, 0.2)',
                                            ];
                                            var bordercolor = [
                                                'rgba(255,99,132,1)',
                                                'rgba(54, 162, 235, 1)',
                                                'rgba(255, 206, 86, 1)',
                                                'rgba(75, 192, 192, 1)',
                                                'rgba(153, 102, 255, 1)',
                                                'rgba(255, 159, 64, 1)',
                                                'rgba(0, 139, 139, 0.1)',
                                                'rgba(47, 79, 79, 0.1)',
                                                'rgba(0, 255, 255, 0.1)',
                                                'rgba(34, 139, 34, 0.1)',
                                                'rgba(178, 34, 34, 0.1)',
                                                'rgba(138, 43, 226, 0.1)',
                                                'rgba(222, 184, 135, 0.1)',
                                                'rgba(100, 149, 237, 0.1)',
                                                'rgba(169, 169, 169, 0.1)',
                                                'rgba(139, 0, 139, 0.1)',
                                            ];

                                            for (var i in data) {
                                                description.push(data[i].description);
                                                users.push(data[i].users);
                                            }

                                            var chartdata = {
                                                labels: description,
                                                datasets: [{
                                                        label: "mes",
                                                        backgroundColor: color,
                                                        borderColor: color,
                                                        borderWidth: 2,
                                                        hoverBackgroundColor: color,
                                                        hoverBorderColor: bordercolor,
                                                        data: users
                                                    },
                                                ]
                                            };
                                            var mostrar = document.getElementById('myChart4');

                                            var grafico = new Chart(mostrar, {
                                                type: 'line',
                                                data: chartdata,
                                                options: {
                                                    scales: {
                                                        yAxes: [{
                                                            ticks: {
                                                                beginAtZero: true
                                                            }
                                                        }]
                                                    },
                                                    responsive: true,
                                                }
                                            });
                                        },
                                        error: function(data) {
                                            console.log(data);
                                        }
                                    });
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
        </div>
    </div>
</div>



<?php require 'views/templates/footer.php' ?>