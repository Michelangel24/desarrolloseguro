<?php require 'views/templates/header.php' ?>
<br>
<br>
<?php
$mensaje = "";
echo $this->mensaje;
?>
<div class="container glass">
    <div class="row">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Procesos</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Empresa</button>
            </li>
            <!-- <li class="nav-item" role="presentation">
                <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Fecha</button>
            </li> -->
        </ul>
        <div class="container-fluid">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <br>
                    <div class="accordion" id="accordionExample">
                        <?php foreach ($this->processes as $row) {
                            $process = new Processes();
                            $process = $row;
                        ?>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading<?php echo $process->idprocess ?>">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo $process->idprocess ?>" aria-expanded="false" aria-controls="collapse<?php echo $process->idprocess ?>">
                                        <?php echo   $process->description ?></option>
                                    </button>
                                </h2>
                                <div id="collapse<?php echo $process->idprocess ?>" class="accordion-collapse collapse" aria-labelledby="heading<?php echo $process->idprocess ?>" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">Empresa</th>
                                                    <th scope="col">Situacion</th>
                                                    <th scope="col">Privilegios</th>
                                                    <th scope="col">Fecha de creacion</th>
                                                    <th scope="col">Descarga</th>
                                                    <th scope="col">Accion</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($this->lessons as $row) {
                                                    $lesson = new Lessons();
                                                    $lesson = $row;
                                                    if ($lesson->process_idprocess == $process->idprocess) {
                                                ?>
                                                        <tr>
                                                            <th scope="row"><?php echo $lesson->idlearned_lesson; ?></th>
                                                            <td><?php echo $lesson->desc_company; ?></td>
                                                            <td><?php echo $lesson->situation; ?></td>
                                                            <td><?php echo $lesson->desc_lesson_type; ?></td>
                                                            <td><?php echo $lesson->date_insert; ?></td>
                                                            <td>
                                                                <small>
                                                                    <div class="row" style="margin-right: auto; margin-left: auto; place-content: center; min-inline-size: max-content;">
                                                                        <div class="col">
                                                                            <a class="material-icons icon" download id="btn_<?php echo $lesson->idlearned_lesson; ?>" href="<?php echo constant('URL') . 'lesson/create_pdf/' . $lesson->idlearned_lesson; ?>">
                                                                                file_download
                                                                            </a>
                                                                        </div>
                                                                        <div class="col">
                                                                            <a class="material-icons icon" download id="btn_<?php echo $lesson->idlearned_lesson; ?>" href="<?php echo $lesson->attached_file; ?>">
                                                                                attach_file
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </small>
                                                            </td>
                                                            <td>
                                                                <small>
                                                                    <div class="row" style="margin-right: auto; margin-left: auto; place-content: center; min-inline-size: max-content;">
                                                                        <div class="col">
                                                                            <a class="material-icons icon" id="btn_<?php echo $lesson->idlearned_lesson; ?>" href="<?php echo constant('URL') . 'lesson/detail/' . $lesson->idlearned_lesson; ?>">
                                                                                visibility
                                                                            </a>
                                                                        </div>
                                                                        <div class="col">
                                                                            <a class="material-icons icon" id="btn_<?php echo $lesson->idlearned_lesson; ?>" href="<?php echo constant('URL') . 'lesson/edit/' . $lesson->idlearned_lesson; ?>">
                                                                                edit
                                                                            </a>
                                                                        </div>
                                                                        <div class="col">
                                                                            <a class="material-icons icon" download id="btn_<?php echo $lesson->idlearned_lesson; ?>" href="<?php echo $lesson->document; ?>">
                                                                                delete
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </small>
                                                            </td>
                                                        </tr>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <br>
                    <div class="accordion" id="accordionCompany">
                        <?php foreach ($this->companies as $row) {
                            $company = new Companies();
                            $company = $row;
                        ?>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingCompany<?php echo $company->idcompany ?>">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseCompany<?php echo $company->idcompany ?>" aria-expanded="false" aria-controls="collapseCompany<?php echo $company->idcompany ?>">
                                        <?php echo   $company->description ?></option>
                                    </button>
                                </h2>
                                <div id="collapseCompany<?php echo $company->idcompany ?>" class="accordion-collapse collapse" aria-labelledby="headingCompany<?php echo $company->idcompany ?>" data-bs-parent="#accordionCompany">
                                    <div class="accordion-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">Proceso</th>
                                                    <th scope="col">Situacion</th>
                                                    <th scope="col">Privilegios</th>
                                                    <th scope="col">Fecha de creacion</th>
                                                    <th scope="col">Descarga</th>
                                                    <th scope="col">Accion</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($this->lessons as $row) {
                                                    $lesson = new Lessons();
                                                    $lesson = $row;
                                                    if ($lesson->company_idcompany == $company->idcompany) {
                                                ?>
                                                        <tr>
                                                            <th scope="row"><?php echo $lesson->idlearned_lesson; ?></th>
                                                            <td><?php echo $lesson->desc_process; ?></td>
                                                            <td><?php echo $lesson->situation; ?></td>
                                                            <td><?php echo $lesson->desc_lesson_type; ?></td>
                                                            <td><?php echo $lesson->date_insert; ?></td>
                                                            <td>
                                                                <small>
                                                                    <div class="row" style="margin-right: auto; margin-left: auto; place-content: center; min-inline-size: max-content;">
                                                                        <div class="col">
                                                                            <a class="material-icons icon" download id="btn_<?php echo $lesson->idlearned_lesson; ?>" href="<?php echo $lesson->document; ?>">
                                                                                file_download
                                                                            </a>
                                                                        </div>
                                                                        <div class="col">
                                                                            <a class="material-icons icon" download id="btn_<?php echo $lesson->idlearned_lesson; ?>" href="<?php echo $lesson->document; ?>">
                                                                                attach_file
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </small>
                                                            </td>
                                                            <td>
                                                                <small>
                                                                    <div class="row" style="margin-right: auto; margin-left: auto; place-content: center; min-inline-size: max-content;">
                                                                        <div class="col">
                                                                            <a class="material-icons icon" id="btn_<?php echo $lesson->idlearned_lesson; ?>" href="<?php echo constant('URL') . 'lesson/detail/' . $lesson->idlearned_lesson; ?>">
                                                                                visibility
                                                                            </a>
                                                                        </div>
                                                                        <div class="col">
                                                                            <a class="material-icons icon" download id="btn_<?php echo $lesson->idlearned_lesson; ?>" href="<?php echo $lesson->document; ?>">
                                                                                edit
                                                                            </a>
                                                                        </div>
                                                                        <div class="col">
                                                                            <a class="material-icons icon" download id="btn_<?php echo $lesson->idlearned_lesson; ?>" href="<?php echo $lesson->document; ?>">
                                                                                delete
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </small>
                                                            </td>
                                                        </tr>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        <?php
                        }
                        ?>
                    </div>
                </div>
                <!-- <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div> -->
            </div>
        </div>
    </div>
    <br>

</div>