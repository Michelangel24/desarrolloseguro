<?php require 'views/templates/header.php' ?>
<br>
<br>
<div class="container glass">
    <div class="container-fluid">
        <br>

        <?php
        $mensaje = "";
        echo $this->mensaje;
        ?>
        <form action="<?php echo constant('URL'); ?>lesson/save" method="POST" enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <label for="company" class="form-label">Empresa</label>
                    <select class="form-select" aria-label="Seleccionar empresa" name="company" id="company" required>
                        <option hidden value="" selected>Selecciona una empresa</option>
                        <?php foreach ($this->companies as $row) {
                            $company = new Companies();
                            $company = $row;
                        ?>
                            <option value="<?php echo $company->idcompany ?>">
                                <?php echo   $company->description ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-sm-12 col-md-6">
                    <label for="process" class="form-label">Proceso</label>
                    <select class="form-select" name="process" id="process" required="" aria-required="true">
                        <option value="" disabled selected>Seleccione una opcion</option>
                        <?php foreach ($this->processes as $row) {
                            $process = new Processes();
                            $process = $row;
                        ?>
                            <option value="<?php echo $process->idprocess ?>">
                                <?php echo $process->description ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>

            <br>

            <hr>

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="form-floating">
                        <textarea class="form-control" placeholder="Commentarios" name="situation" id="situation"></textarea>
                        <label for="situation">Situación presentada</label>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-floating">
                        <textarea class="form-control" placeholder="Causas" name="cause" id="cause" data-length="512"></textarea>
                        <label for="cause">Causas</label>
                    </div>
                </div>
            </div>

            <br>

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="form-floating">
                        <textarea class="form-control" placeholder="Causas" name="lesson" id="lesson" data-length="512"></textarea>
                        <label for="lesson">¿Cual es la leccion aprendida especificamente?</label>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-floating">
                        <!-- <label for="company" class="form-label">Privilegios</label> -->
                        <select class="form-select" aria-label="Seleccionar privilegios" name="lesson_type" id="lesson_type" required>
                            <option hidden value="" selected>Selecciona quien puede acceder a la leccion</option>
                            <?php foreach ($this->lessontypes as $row) {
                                $lessontype = new LessonTypes();
                                $lessontype = $row;
                            ?>
                                <option value="<?php echo $lessontype->idlesson_type ?>">
                                    <?php echo   $lessontype->description ?></option>
                            <?php
                            }
                            ?>
                        </select>
                        <!-- <textarea class="form-control" placeholder="Causas" name="interested" id="interested" data-length="512"></textarea> -->
                        <label for="lesson_type">Privilegios - ¿Quien deberia estar informado de la leccion aprendida?</label>

                    </div>
                </div>
            </div>

            <br>


            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="form-floating">
                        <textarea class="form-control" placeholder="Causas" name="use" id="use" data-length="512"></textarea>
                        <label for="use">¿Donde y como puede utilizarse este conocimiento a futuro?</label>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-floating">
                        <textarea class="form-control" placeholder="Causas" name="diffusion" id="diffusion" data-length="512"></textarea>
                        <label for="diffusion">¿Como deberia ser difundida la leccion aprendida?</label>
                    </div>
                </div>
            </div>

            <br>

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="form-floating">
                        <textarea class="form-control" placeholder="document" name="document" id="document" data-length="512"></textarea>
                        <label for="document">¿Qué documentos del SGI deberían modificarse?</label>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6" style="margin-top: auto;">
                    <input class="form-control" placeholder="Cargar un documento" type="file" name="file" id="file" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                                text/plain, application/pdf">
                </div>
            </div>
            <br>
            <br>
            <div style="text-align: center">
                <button class="btn btn-outline-success" type="submit" name="action">Guardar
                    <i class="material-icons right" style="vertical-align: middle;">send</i>
            </div>
            </button>

        </form>
        <br>

    </div>

    <script>
        autosize(document.querySelectorAll('textarea'));
    </script>
</div>