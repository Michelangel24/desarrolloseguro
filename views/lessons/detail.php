<?php require 'views/templates/header.php' ?>

<br>
<br>



<div class="container">
    <?php
    // print_r($this);
    $lesson = $this->lessons[0];
    // print_r($lesson);
    ?>

    <div class="card glass">
        <h5 class="card-header">Reporte <?php echo $lesson->idlearned_lesson ?></h5>
        <div class="card-body">
            <?php
            $mensaje = "";
            echo $this->mensaje;
            // print_r($this);
            ?>

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <strong><label for="company">Empresa</label></strong>
                    <p>
                        <?php echo $lesson->desc_company ?>
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <strong><label for="process">Proceso</label></strong>
                    <p>
                        <?php echo $lesson->desc_process ?>
                    </p>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <strong><label for="situation">Situación presentada</label></strong>
                    <p>
                        <?php echo $lesson->situation ?>
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <strong><label for="cause">Causas</label></strong>
                    <p>
                        <?php echo $lesson->cause ?>
                    </p>
                </div>
            </div>

            <br>

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <strong><label for="lesson">¿Cual es la leccion aprendida especificamente?</label></strong>
                    <p>
                        <?php echo $lesson->lesson ?>
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <strong><label for="lesson_type">Privilegios - ¿Quien deberia estar informado de la leccion aprendida?</label></strong>
                    <p>
                        <?php echo $lesson->desc_lesson_type ?>
                    </p>
                </div>
            </div>

            <br>


            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <strong><label for="use">¿Donde y como puede utilizarse este conocimiento a futuro?</label></strong>
                    <p>
                        <?php echo $lesson->practice ?>
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <strong><label for="diffusion">¿Como deberia ser difundida la leccion aprendida?</label></strong>
                    <p>
                        <?php echo $lesson->diffusion ?>
                    </p>
                </div>
            </div>

            <br>

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <strong><label for="document">¿Qué documentos del SGI deberían modificarse?</label></strong>
                    <p>
                        <?php echo $lesson->document;?>
                    </p>
                </div>
                <div class="col-sm-12 col-md-6">
                    <strong><label for="document">Archivo adjunto</label></strong>
                    <p>
                        <a class="material-icons" style="font-size: 1rem;" download href="<?php echo $lesson->attached_file; ?>">
                            attach_file
                        </a>
                        <a href="<?php echo $lesson->attached_file ?>" download>
                            <?php
                            $url = $lesson->attached_file;
                            $archivo = basename($url);
                            echo $archivo;
                            ?>
                        </a>
                    </p>
                </div>
            </div>

        </div>
    </div>

</div>

<?php require 'views/templates/footer.php' ?>