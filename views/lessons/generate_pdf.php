<?php

// ob_start();

// if(!isset($_SESSION)) 
// { 
//     session_start(); 
//   } 
//       if ($_SESSION['rol'] != '3') {
//           header("location:".constant('URL')); 
//       }


// 
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>public/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo constant('URL'); ?>public/css/styles.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- JS -->
    <script type="text/javascript" src="<?php echo constant('URL'); ?>public/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="<?php echo constant('URL'); ?>public/js/bootstrap.min.js"></script>

    <title>Document</title>

    <style>
        td {
            width: 6.25% !important;
            height: 20px;
        }

        body {
            margin: 3%;
            margin-top: 1%;
            font-family: verdana;
            font-size: 14px;
        }
    </style>

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col">
                hola
            </div>
            <div class="col">
                Mundo
            </div>
        </div>
    </div>

    <div class="container-fluid">

        <table style="border: black 1px solid; width: 100%;">
            <tr>
                <td colspan="10">
                    <h1>Formato de lecciones aprendidas</h1>
                </td>
                <td colspan="6"></td>
            </tr>

        </table>



    </div>


</body>


</html>

<?php

require 'vendor/autoload.php';
// require_once 'dompdf/autoload.inc.php';

use Dompdf\Dompdf;
# Cargamos la librería dompdf.
// require_once 'dompdf/dompdf_config.inc.php';

# Instanciamos un objeto de la clase DOMPDF.
$mipdf = new DOMPDF();

# Definimos el tamaño y orientación del papel que queremos.
# O por defecto cogerá el que está en el fichero de configuración.
$mipdf->set_paper("a4", "portrait");

# Cargamos el contenido HTML.
$mipdf->load_html(ob_get_clean());

# Renderizamos el documento PDF.
$mipdf->render();

# Enviamos el fichero PDF al navegador.
$mipdf->stream("Clientes.pdf");

?>