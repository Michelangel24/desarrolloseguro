<?php require 'views/templates/header.php' ?>
<br>
<br>
<div class="container">
    <?php
    $mensaje = "";
    echo $this->mensaje;
    ?>


    <div class="card">
        <h5 class="card-header">Agregar usuario</h5>
        <div class="card-body">
            <form action="<?php echo constant('URL'); ?>user/save" method="POST">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <label for="iduser" class="form-label">Cedula</label>
                        <input type="number" name="iduser" id="iduser" class="form-control" placeholder="Cedula del usuario" maxlength="10" aria-label="Cedula del usuario" required>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <label for="process" class="form-label">Proceso</label>
                        <select class="form-select" name="process" id="process" required="" aria-required="true">
                            <option value="" disabled selected>Seleccione una opcion</option>
                            <?php foreach ($this->processes as $row) {
                                $process = new Processes();
                                $process = $row;
                            ?>
                                <option value="<?php echo $process->idprocess ?>">
                                    <?php echo $process->description ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <label for="name" class="form-label">Nombre</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nombre del usuario" maxlength="50" aria-label="Nombre del usuario" onkeyup="javascript:this.value=this.value.toUpperCase();" required>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <label for="surname" class="form-label">Apellido</label>
                        <input type="text" name="surname" id="surname" class="form-control" placeholder="Apellido del usuario" maxlength="50" aria-label="Apellido del usuario" onkeyup="javascript:this.value=this.value.toUpperCase();" required>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <label for="phone" class="form-label">Telefono</label>
                        <input type="number" name="phone" id="phone" class="form-control" placeholder="Telefono del usuario" maxlength="50" aria-label="Telefono del usuario" required>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <label for="email" class="form-label">Correo electronico</label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="Correo del usuario" maxlength="50" aria-label="Correo del usuario" required>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group no_border">
                            <label for="rol" class="form-label">Rol</label>
                            <select class="form-select" aria-label="Default select example" name="rol" id="rol" required>
                                <option hidden value="" selected>Selecciona una opción</option>
                                <?php foreach ($this->roles as $row) {
                                    $rol = new Roles();
                                    $rol = $row;
                                ?>
                                    <option value="<?php echo $rol->idrol ?>">
                                        <?php echo  $rol->description ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group no_border">
                            <label for="company" class="form-label">Empresa</label>
                            <select class="form-select" aria-label="Default select example" name="company" id="company" required>
                                <option hidden value="" selected>Selecciona una opción</option>
                                <?php foreach ($this->companies as $row) {
                                    $company = new Companies();
                                    $company = $row;
                                ?>
                                    <option value="<?php echo $company->idcompany ?>">
                                        <?php echo   $company->description ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="d-grid gap-2 col-6 mx-auto">
                    <button class="btn btn-outline-success" type="submit">Crear</button>
                </div>
            </form>
        </div>
    </div>

</div>

<?php require 'views/templates/footer.php' ?>