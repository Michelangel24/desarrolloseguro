<?php require 'views/templates/header.php' ?>

<div class="container">
    <?php
    $mensaje = "";
    echo $this->mensaje;
    ?>


    <div class="card">
        <h5 class="card-header">Agregar usuario</h5>
        <div class="card-body">
            <form action="<?php echo constant('URL'); ?>user/update" method="POST">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <label for="iduser" class="form-label">Cedula</label>
                        <input type="number" name="iduser" id="iduser" class="form-control"
                            placeholder="Cedula del usuario" maxlength="10" aria-label="Cedula del usuario"
                            value="<?php echo $this->users->iduser; ?>" disabled required>
                        <input type="hidden" name="iduser" id="iduser" value="<?php echo $this->users->iduser; ?>" />
                    </div>
                    <div class="col-sm-12 col-md-6">
                    <label for="process" class="form-label">Proceso</label>
                    <select class="form-select" name="process" id="process" required="" aria-required="true">
                        <?php foreach ($this->processes as $row) {
                            $process = new Processes();
                            $process = $row;
                        ?>
                            <option value="<?php echo $process->idprocess ?>" <?php if ($process->idprocess == $this->users->process_idprocess) echo 'selected'; ?>>
                                <?php echo $process->description ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                </div>
                <br>
                <div class=" row">
                    <div class="col-sm-12 col-md-6">
                        <label for="name" class="form-label">Nombre</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nombre del usuario"
                            maxlength="50" aria-label="Nombre del usuario"
                            onkeyup="javascript:this.value=this.value.toUpperCase();"
                            value=" <?php echo $this->users->name; ?>" required>
                    </div>
                    <div class=" col-sm-12 col-md-6">
                        <label for="surname" class="form-label">Apellido</label>
                        <input type="text" name="surname" id="surname" class="form-control"
                            placeholder="Apellido del usuario" maxlength="50" aria-label="Apellido del usuario"
                            onkeyup="javascript:this.value=this.value.toUpperCase();"
                            value="<?php echo $this->users->surname; ?>" required>
                    </div>
                </div>
                <br>
                <div class=" row">
                    <div class="col-sm-12 col-md-6">
                        <label for="phone" class="form-label">Telefono</label>
                        <input type="number" name="phone" id="phone" class="form-control"
                            placeholder="Telefono del usuario" maxlength="50" aria-label="Telefono del usuario"
                            value="<?php echo $this->users->phone; ?>" required>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <label for="email" class="form-label">Correo electronico</label>
                        <input type="email" name="email" id="email" class="form-control"
                            placeholder="Correo del usuario" maxlength="50" aria-label="Correo del usuario"
                            value="<?php echo $this->users->email; ?>" required>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group no_border">
                            <label for="rol" class="form-label">Rol</label>
                            <select class="form-select" aria-label="Default select example" name="rol" id="rol"
                                required>
                                <?php foreach ($this->roles as $row) {
                                    $rol = new Roles();
                                    $rol = $row;
                                ?>
                                <option value="<?php echo $rol->idrol ?>"
                                    <?php if ($rol->idrol == $this->users->rol_idrol) echo 'selected'; ?>>
                                    <?php echo   $rol->description ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group no_border">
                            <label for="company" class="form-label">Empresa</label>
                            <select class="form-select" aria-label="Default select example" name="company" id="company"
                                required>
                                <?php foreach ($this->companies as $row) {
                                    $company = new Companies();
                                    $company = $row;
                                ?>
                                <option value="<?php echo $company->idcompany ?>"
                                    <?php if ($company->idcompany == $this->users->company_idcompany) echo 'selected'; ?>>
                                    <?php echo   $company->description ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="d-grid gap-2 col-6 mx-auto">
                    <button class="btn btn-outline-success" type="submit">Actualizar</button>
                </div>
            </form>
        </div>
    </div>

</div>

<?php require 'views/templates/footer.php' ?>