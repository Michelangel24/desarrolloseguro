<?php require 'views/templates/header.php' ?>

<br>
<br>

<div class="container">

    <div class="card">
        <h5 class="card-header">listado de usuarios</h5>
        <div class="card-body">
            <?php
            $mensaje = "";
            echo $this->mensaje;
            ?>

            <form action="<?php echo constant('URL'); ?>bus/save" method="POST">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Cedula</th>
                                <th scope="col">Proceso</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Apellido</th>
                                <th scope="col">Telefono</th>
                                <th scope="col">Email</th>
                                <th scope="col">rol</th>
                                <th scope="col">Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($this->users as $row) {
                                $user = new Users();
                                $user = $row;
                            ?>
                                <tr>
                                    <th scope="row"><?php echo $user->iduser; ?></th>
                                    <td><?php echo $user->process_idprocess; ?></td>
                                    <td><?php echo $user->name; ?></td>
                                    <td><?php echo $user->surname; ?></td>
                                    <td><?php echo $user->phone; ?></td>
                                    <td><?php echo $user->email; ?></td>
                                    <td><?php echo $user->rol_idrol; ?></td>
                                    <td>
                                        <a class="material-icons icon" href="<?php echo constant('URL') . 'user/edit/'  . $user->iduser; ?>">
                                            edit
                                        </a>

                                        <a class="material-icons icon" href="" data-bs-toggle="modal" data-bs-target="#exampleModal<?php echo $user->iduser ?>">
                                            backspace
                                        </a>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal<?php echo $user->iduser ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Eliminar usuario</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        Realmente desea eliminar al usuario <?php echo $user->name ?>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-success" data-bs-dismiss="modal">Cancelar</button>
                                                        <button type="button" onclick="location.href='<?php echo constant('URL') . 'user/delete/' . $user->iduser; ?>'" class="btn btn-danger">Eliminar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <a class="material-icons icon" href="" data-bs-toggle="modal" data-bs-target="#newpass<?php echo $user->iduser ?>">
                                            settings_backup_restore
                                        </a>

                                        <!-- Modal -->
                                        <div class="modal fade" id="newpass<?php echo $user->iduser ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Reestablecer
                                                            contraseña</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        Realmente desea reestablecer la contraseña del usuario
                                                        <?php echo $user->name ?>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-success" data-bs-dismiss="modal">Cancelar</button>
                                                        <button type="button" onclick="location.href='<?php echo constant('URL') . 'user/restorepass/' . $user->iduser; ?>'" class="btn btn-danger">Reestablecer</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>

</div>

<?php require 'views/templates/footer.php' ?>