-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-05-2021 a las 03:50:25
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `securedevelop`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company`
--

CREATE TABLE `company` (
  `idcompany` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `company`
--

INSERT INTO `company` (`idcompany`, `description`, `comment`) VALUES
(1, 'SI18 Calle 80', NULL),
(2, 'SI18 Norte', NULL),
(3, 'SI18 Suba', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lesson_learned`
--

CREATE TABLE `lesson_learned` (
  `idlearned_lesson` int(11) NOT NULL,
  `date_insert` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_iduser` int(11) NOT NULL,
  `company_idcompany` int(11) NOT NULL,
  `situation` varchar(512) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `cause` varchar(512) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `lesson` varchar(512) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `practice` varchar(512) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `diffusion` varchar(512) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `process_idprocess` int(11) NOT NULL,
  `lesson_type_idlesson_type` int(11) NOT NULL,
  `document` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `attached_file` varchar(255) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `lesson_learned`
--

INSERT INTO `lesson_learned` (`idlearned_lesson`, `date_insert`, `user_iduser`, `company_idcompany`, `situation`, `cause`, `lesson`, `practice`, `diffusion`, `process_idprocess`, `lesson_type_idlesson_type`, `document`, `attached_file`) VALUES
(1, '2021-04-20 06:14:56', 1019093071, 1, 'tyertyerty', 'dfgsdrrgsdfg', 'asdfasdfasdf', 'rtsdfgsdfg', 'sdftggwertwetr', 2, 1, '', ''),
(2, '2021-04-20 06:14:56', 1019093071, 1, 'hhrsghsfshrtyefgh', 'dfghdcvbxcvb', 'dfgherthdfgh', 'tsdfgxcvbxcvb', 'sdfgadsfzxdfz', 2, 1, '', ''),
(7, '2021-04-20 06:14:56', 1019093071, 1, 'ertwertwet', 'wertwertwetr', 'ertwertert', 'wtrwertwert', 'wertwertwert', 2, 1, '', ''),
(8, '2021-04-20 06:14:56', 1019093071, 1, 'asdasdas', 'asdasdasd', 'asdasdasd', 'asdasdasd', 'asdasdasdasd', 2, 1, '', ''),
(10, '2021-04-20 06:14:56', 1019093071, 2, 'asdfasdfasdf', 'asdfasdf<zxc', 'fdgsdfgeret', 'zxcvafdasdfas', 'vcxvgbxcvb', 2, 1, '', ''),
(11, '2021-04-20 06:14:56', 1019093071, 2, 'texto al azar', 'texto al azar', 'texto al azar', 'texto al azar', 'texto al azar', 3, 1, '', ''),
(13, '2021-04-20 06:14:56', 1019093071, 3, 'multa por parte de trasnmilenio', 'incumplimiento de horarios en estaciones', 'mejorar la puntualidad', 'en todas las empresas', 'a traves de campañas en todos los patios', 1, 2, 'http://localhost/proyects/leccionesAprendidas/resources/documents/Document_09042021-025958.pdf', ''),
(14, '2021-04-20 06:14:56', 1019093071, 2, 'Desperdicio de agua en el lavado de los vehículos', 'No reutilización de agua', 'Conectar la llave de suministro de agua de lavado directamente al agua potable', 'En todos los aspectos de la vida', 'Campañas en el área de lavado', 4, 4, 'http://localhost/proyects/leccionesAprendidas/resources/documents/Document_09042021-114323.pdf', ''),
(15, '2021-04-20 06:14:56', 1019093071, 2, 'weqweqwertee', 'fdsgsefrtert', 'cxvfsdfwerfsdf', 'sdfwferwer', 'fsdfcvxcvsdfs', 2, 3, 'http://localhost/proyects/leccionesAprendidas/resources/documents/Document_20042021-010218.pdf', ''),
(16, '2021-04-20 06:14:56', 1019093071, 2, 'weqweqwertee', 'fdsgsefrtert', 'cxvfsdfwerfsdf', 'sdfwferwer', 'fsdfcvxcvsdfs', 2, 3, 'http://localhost/proyects/leccionesAprendidas/resources/documents/Document_20042021-010450.pdf', ''),
(17, '2021-04-20 06:14:56', 1019093071, 2, 'hola', 'mmundo', 'ninguna', 'prueba', 'test', 2, 1, 'http://localhost/proyects/leccionesAprendidas/resources/documents/Document_20042021-010737.pdf', ''),
(18, '2021-04-20 06:19:19', 1019093071, 3, 'esto es una prueba', 'esto es una prueba', 'esto es una prueba', 'esto es una prueba', 'esto es una prueba', 1, 3, 'http://localhost/proyects/leccionesAprendidas/resources/documents/Document_20042021-011919.pdf', ''),
(19, '2021-04-20 06:24:05', 1019093071, 1, 'esto es otra prueba', 'esto es otra prueba', 'esto es otra prueba', 'esto es otra prueba', 'esto es otra prueba', 2, 3, 'http://localhost/proyects/leccionesAprendidas/resources/documents/Document_20042021-012405.pdf', ''),
(20, '2021-04-20 06:28:09', 1019093071, 2, 'hola, esto es otra prueba', 'hola, esto es otra prueba', 'hola, esto es otra prueba', 'hola, esto es otra prueba', 'hola, esto es otra prueba', 3, 3, 'http://localhost/proyects/leccionesAprendidas/resources/documents/Document_20042021-012809.pdf', ''),
(21, '2021-04-20 07:26:06', 1019093071, 2, 'erwerwer', 'werwerwer', 'werwerwer', 'werwerwerwer', 'werwerwerwer', 3, 3, 'werwerwerwer', 'http://localhost/proyects/leccionesAprendidas/resources/documents/Document_20042021-022606.pdf'),
(22, '2021-04-22 16:13:54', 1019093071, 2, 'ertwetr', 'gsdfgsdfg', 'qwerqwerew', 'asdfasdf', 'zxcvzxcv', 4, 3, 'wertwertwret', 'http://localhost/proyects/leccionesAprendidas/resources/documents/Document_22042021-111354.pdf'),
(23, '2021-05-10 05:25:50', 1019093071, 2, 'jgdhffghgm', 'dfsdfsdfz', 'czxcwerfsdf', 'sdfgwergsf', 'zcvasdfgsdfg', 1, 3, 'dfgwsdfzxzvc', 'http://localhost/proyects/leccionesAprendidasDesarrolloSeguro/resources/documents/Document_10052021-002550.pdf'),
(24, '2021-05-12 18:56:38', 1019093071, 2, 'qwerqwer', 'qwerqwerqwer', 'qwerqwerqwer', 'qwerqwer', 'qwerqwerqwer', 2, 2, 'qwerqwerqwer', 'http://localhost/proyects/leccionesAprendidasDesarrolloSeguro/resources/documents/Document_12052021-135638.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lesson_type`
--

CREATE TABLE `lesson_type` (
  `idlesson_type` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `lesson_type`
--

INSERT INTO `lesson_type` (`idlesson_type`, `description`, `comment`) VALUES
(1, 'publico', NULL),
(2, 'privado - todas empresas', NULL),
(3, 'privado - empresa', NULL),
(4, 'privado - proceso', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `idlogin` int(11) NOT NULL,
  `user_iduser` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`idlogin`, `user_iduser`, `password`, `token`) VALUES
(1, 1019093071, '$2y$10$34chzvk9DRt6kcIzvo9wzOp0gbQt/csyCpk9u0x4vb.2DxGg4CZIK', '$2y$10$dJgxmu02VEANHxRgG8LKg.P4t4s/iQL87SmBph.ye2XaHTii4U4/W'),
(4, 6575675, '$2y$10$0vlIY76Gh5VaYuF7vzBb5e9F.HtLxYNjm3Y2A2ZA1HKqGlogC6rZ6', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `process`
--

CREATE TABLE `process` (
  `idprocess` int(11) NOT NULL,
  `description` varchar(64) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `process`
--

INSERT INTO `process` (`idprocess`, `description`, `comment`) VALUES
(1, 'Gestión Humana', NULL),
(2, 'Tecnologías de la información y la comunicación', NULL),
(3, 'Sostenibilidad', NULL),
(4, 'Gestión ambiental', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `idrol` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idrol`, `description`, `comment`) VALUES
(1, 'Administrador', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `surname` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `phone` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `rol_idrol` int(11) NOT NULL,
  `company_idcompany` int(11) NOT NULL,
  `process_idprocess` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`iduser`, `name`, `surname`, `phone`, `email`, `rol_idrol`, `company_idcompany`, `process_idprocess`) VALUES
(6575675, 'YTRYRTYTR', 'YTRYRTYTR', '3138331374', 'miguelcasas1994@gmail.com', 1, 3, 2),
(1019093071, 'Miguel', 'Casas', '3213249847', 'mcasas@si18.com.co', 1, 3, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`idcompany`);

--
-- Indices de la tabla `lesson_learned`
--
ALTER TABLE `lesson_learned`
  ADD PRIMARY KEY (`idlearned_lesson`),
  ADD KEY `fk_learned_lesson_company1_idx` (`company_idcompany`),
  ADD KEY `fk_learned_lesson_user1_idx` (`user_iduser`),
  ADD KEY `fk_lesson_learned_process1_idx` (`process_idprocess`),
  ADD KEY `fk_lesson_learned_lesson_type1_idx` (`lesson_type_idlesson_type`);

--
-- Indices de la tabla `lesson_type`
--
ALTER TABLE `lesson_type`
  ADD PRIMARY KEY (`idlesson_type`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`idlogin`),
  ADD KEY `fk_table1_user1_idx` (`user_iduser`);

--
-- Indices de la tabla `process`
--
ALTER TABLE `process`
  ADD PRIMARY KEY (`idprocess`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idrol`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`),
  ADD UNIQUE KEY `iduser_UNIQUE` (`iduser`),
  ADD KEY `fk_user_rol_idx` (`rol_idrol`),
  ADD KEY `fk_user_company1_idx` (`company_idcompany`),
  ADD KEY `fk_user_process1_idx` (`process_idprocess`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `company`
--
ALTER TABLE `company`
  MODIFY `idcompany` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `lesson_learned`
--
ALTER TABLE `lesson_learned`
  MODIFY `idlearned_lesson` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `lesson_type`
--
ALTER TABLE `lesson_type`
  MODIFY `idlesson_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `login`
--
ALTER TABLE `login`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `process`
--
ALTER TABLE `process`
  MODIFY `idprocess` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `idrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
