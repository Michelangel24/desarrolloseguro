$(document).ready(function() {

    $('select').formSelect();

    $('input#input_text, textarea#textarea2').characterCounter();

    $("select[required]").css({ display: "block", height: 0, padding: 0, width: 0, position: 'absolute' });

});