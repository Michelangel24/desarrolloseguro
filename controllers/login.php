<?php

require_once 'libs/controller.php';

require 'vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

class Login extends Controller
{

    function __construct()
    {
        parent::__construct();
        $this->view->mensaje = "";
        $this->view->mensaje_1 = "";
        $this->view->mensaje_2 = "";
    }

    function render()
    {
        // $mensaje = "";

        // $users = $this->model->show();
        // $mensaje = '';
        // $this->view->users = $users;
        $this->view->render('home/login');
    }

    function loginUser()
    {

        $iduser = $_POST['user'];
        $pass   = $_POST['password'];

        $session = $this->model->login([
            'iduser' => $iduser,
        ]);
        if (isset($session->iduser)) {
            $password = $session->password;
            if (password_verify($pass, $password)) {
                $token = $this->random_password();
                $this->sendMails($session->email, $session->name, $token);
                $tokenenc = password_hash($token, PASSWORD_DEFAULT);
                if ($this->model->createToken([
                    'user_iduser'   => $iduser,
                    'token'         => $tokenenc
                ])){
                    $this->view->session = $session;
                    $this->view->render('home/token');
                }else{
                    $this->view->mensaje = "Token invalido";
                    $this->view->render('home/index');
                }
            } else {
                $mensaje_1 = '';
                $mensaje_2 = 'Contraseña incorrecta';
                $this->view->mensaje_1 = $mensaje_1;
                $this->view->mensaje_2 = $mensaje_2;
                $this->render();
            }
        } else {
            $mensaje_1 = 'Usuario no registrado';
            $mensaje_2 = '';
            $this->view->mensaje_1 = $mensaje_1;
            $this->view->mensaje_2 = $mensaje_2;
            $this->render();
        }
    }

    function verifyToken()
    {

        $iduser = $_POST['iduser'];
        $token  = $_POST['token'];

        $session = $this->model->login([
            'iduser' => $iduser,
        ]);
        if (isset($session->iduser)) {
            $tokenenc = $session->token;
            if (password_verify($token, $tokenenc)) {
                session_start();
                $_SESSION['iduser']         = $session->iduser;
                $_SESSION['name']           = $session->name;
                $_SESSION['rol_idrol']      = $session->rol_idrol;
                $_SESSION["timeout"]        = time();

                // print_r($_SESSION);
                $this->view->render('home/index');
            } else {
                $mensaje_1 = '';
                $mensaje_2 = '';
                $this->view->mensaje = "Token invalido";
                $this->view->mensaje_1 = $mensaje_1;
                $this->view->mensaje_2 = $mensaje_2;
                $this->render();
            }
        } else {
            $mensaje_1 = 'Usuario no registrado';
            $mensaje_2 = '';
            $this->view->mensaje = "Token invalido";
            $this->view->mensaje_1 = $mensaje_1;
            $this->view->mensaje_2 = $mensaje_2;
            $this->render();
        }
    }

    function logout()
    { // Destruir todas las variables de sesión.
        session_start();

        $_SESSION = array();

        // Si se desea destruir la sesión completamente, borre también la cookie de sesión.
        // Nota: ¡Esto destruirá la sesión, y no la información de la sesión!
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(
                session_name(),
                '',
                time() - 42000,
                $params["path"],
                $params["domain"],
                $params["secure"],
                $params["httponly"]
            );
        }
        
        $this->view->render('home/login');
    }

    function sendMails($email, $name, $token)
    {
        // print_r($items);
        // echo !extension_loaded('openssl')?"Not Available":"Available";
        //Crear una instancia de PHPMailer
        $mail = new PHPMailer();
        //Definir que vamos a usar SMTP
        $mail->IsSMTP();
        //Esto es para activar el modo depuración. En entorno de pruebas lo mejor es 2, en producción siempre 0
        // 0 = off (producción)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug  = 0;
        //Ahora definimos gmail como servidor que aloja nuestro SMTP
        $mail->Host       = 'smtp.gmail.com';
        //El puerto será el 587 ya que usamos encriptación TLS
        $mail->Port       = 587;
        //Definmos la seguridad como TLS
        $mail->SMTPSecure = 'tls';
        //Tenemos que usar gmail autenticados, así que esto a TRUE
        $mail->SMTPAuth   = true;

        $mail->SMTPAutoTLS = false;
        //Definimos la cuenta que vamos a usar. Dirección completa de la misma
        $mail->Username   = "soporte@si18.com.co";
        //Introducimos nuestra contraseña de gmail
        $mail->Password   = "*S1Tec99*";
        //Definimos el remitente (dirección y, opcionalmente, nombre)
        $mail->SetFrom('soporte@si18.com.co', 'App Lecciones Aprendidas');
        //Esta línea es por si queréis enviar copia a alguien (dirección y, opcionalmente, nombre)
        // $mail->AddReplyTo('replyto@correoquesea.com','El de la réplica');
        //Y, ahora sí, definimos el destinatario (dirección y, opcionalmente, nombre)
        $mail->AddAddress($email, $name);
        //Definimos el tema del email
        $mail->Subject = 'Acceso App Lecciones Aprendidas';
        $mail->CharSet = 'UTF-8';

        $mail->Body =
            'Buen día Señor ' . $name . ','  . "\n" . "\n" .
            'Su token de acceso a la plataforma Lecciones Aprendidas es:' . "\n" .
            'token: ' . $token  . "\n";
        //Para enviar un correo formateado en HTML lo cargamos con la siguiente función. Si no, puedes meterle directamente una cadena de texto.
        // $mail->MsgHTML(file_get_contents('correomaquetado.html'), dirname(ruta_al_archivo));
        //Y por si nos bloquean el contenido HTML (algunos correos lo hacen por seguridad) una versión alternativa en texto plano (también será válida para lectores de pantalla)
        // $mail->AltBody = 'This is a plain-text message body';
        //Enviamos el correo

        // echo $user->email;
        // print_r($mail);
        if (!$mail->Send()) {
            // echo "Error: " . $mail->ErrorInfo;
            // return false;
        } else {
            // echo "Enviado!";
            // return true;
        }
    }

    function random_password()
    {
        $longitud = 8; // longitud del password  
        $pass = substr(md5(rand()), 0, $longitud);
        return $pass; // devuelve el password   
    }
}
