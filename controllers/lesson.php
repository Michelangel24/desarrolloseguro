<?php

require_once 'libs/controller.php';
require 'vendor/autoload.php';

class Lesson extends Controller
{

    function __construct()
    {
        parent::__construct();
        $this->view->mensaje = "";
    }

    function render()
    {
        $this->view->processes  = $this->model->listProcess();
        $this->view->companies  = $this->model->listCompany();
        $this->view->lessons    = $this->model->listLesson();
        $this->view->render('lessons/index');
    }

    function newlesson()
    {
        $this->view->processes      = $this->model->listProcess();
        $this->view->companies      = $this->model->listCompany();
        $this->view->lessontypes    = $this->model->listLessonType();
        $this->view->render('lessons/create');
    }

    function save()
    {
        // print_r($_POST);
        if ($_FILES['file']['name'] != null) {
            $nombre_img = $_FILES['file']['name'];
            $tipo = $_FILES['file']['type'];
            $tamano = $_FILES['file']['size'];
            $ext = pathinfo($nombre_img, PATHINFO_EXTENSION);

            $time = $this->nameFile();
            $path = 'resources/documents/';
            $name = $path . $time;
            $attached_file = constant('URL') . $name . "." . $ext;

            move_uploaded_file($_FILES['file']['tmp_name'], $name . "." . $ext);
        } else {
            $attached_file = "";
        }

        $company        = $_POST['company'];
        $user           = "1019093071";
        $process        = $_POST['process'];
        $situation      = $_POST['situation'];
        $cause          = $_POST['cause'];
        $lesson         = $_POST['lesson'];
        $practice       = $_POST['use'];
        $diffusion      = $_POST['diffusion'];
        $lesson_type    = $_POST['lesson_type'];
        $document       = $_POST['document'];

        if ($this->model->save([

            'user_iduser'               => $user,
            'company_idcompany'         => $company,
            'process_idprocess'         => $process,
            'situation'                 => $situation,
            'cause'                     => $cause,
            'lesson'                    => $lesson,
            'practice'                  => $practice,
            'diffusion'                 => $diffusion,
            'lesson_type_idlesson_type' => $lesson_type,
            'document'                  => $document,
            'attached_file'             => $attached_file
        ])) {
            $this->view->mensaje = '
            <div class="container">
                <div class="alert alert-success alert-dismissible fade show glass_2" role="alert">
                    Se almaceno su nueva leccion exitosamente
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        ';
            $this->view->processes  = $this->model->listProcess();
            $this->view->companies  = $this->model->listCompany();
            $this->view->lessons    = $this->model->listLesson();
            $this->view->render('lessons/index');
        } else {
            $this->view->mensaje = '
            <div class="container">
                <div class="alert alert-danger alert-dismissible fade show glass_2" role="alert">
                    Ocurrio un error, por favor consulte al administrador
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
            ';
            $this->view->render('lessons/create');
        }
    }

    function edit($param = null)
    {
        $idlesson = $param[0];
        $this->view->lessons        = $this->model->getLessonById($idlesson);
        $this->view->processes      = $this->model->listProcess();
        $this->view->companies      = $this->model->listCompany();
        $this->view->lessontypes    = $this->model->listLessonType();
        $this->view->mensaje        = "";
        $this->view->render('lessons/edit');
    }

    function detail($param = null)
    {
        $idlesson = $param[0];
        $this->view->lessons     = $this->model->getLessonById($idlesson);
        $this->view->render('lessons/detail');
    }

    function nameFile()
    {
        date_default_timezone_set('America/Bogota');
        $time = time();
        $nameprog = "Document_" . date("dmY-His", $time);
        return $nameprog;
    }

    function create_pdf($param = null){

        $idlesson = $param[0];
        $this->view->lessons     = $this->model->getLessonById($idlesson);

        $this->view->render('lessons/generate_pdf');

    }
}
