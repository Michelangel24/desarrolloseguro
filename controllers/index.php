<?php

require_once 'libs/controller.php';

class Index extends Controller
{

    function __construct()
    {
        parent::__construct();
        $this->view->mensaje = "";
    }

    function render()
    {
        $this->view->processes  = $this->model->listProcess();
        $this->view->companies  = $this->model->listCompany();
        $this->view->render('home/index');
    }

}
