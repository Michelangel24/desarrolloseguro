<?php

require_once 'libs/controller.php';

class Statistics extends Controller
{

    function __construct()
    {
        parent::__construct();
        $this->view->mensaje = "";
    }

    function render()
    {
        $this->view->render('statistics/index');
    }

    function company()
    {
        $data = $this->model->list_company();
        print $data;
    }

    function process()
    {
        $data = $this->model->list_process();
        print $data;
    }

    function lessonType()
    {
        $data = $this->model->list_lessonType();
        print $data;
    }

    function dateInsert()
    {
        $data = $this->model->list_dateInsert();
        print $data;
    }

    // function enfermedades()
    // {
    //     if (!isset($_SESSION)) {
    //         session_start();
    //     }
    //     $company = $_SESSION['company'];
    //     if ($_SESSION['profile'] == "4") {
    //         $data  = $this->model->enfermedades();
    //         // $total = $this->model->totalUsers();
    //     } elseif ($_SESSION['profile'] == "1") {
    //         $data = $this->model->enfermedadesAdm($company);
    //     }
    //     print $data;
    //     // print $total;
    // }

    // function report_date()
    // {
    //     if (!isset($_SESSION)) {
    //         session_start();
    //     }
    //     $company = $_SESSION['company'];
    //     if ($_SESSION['profile'] == "4") {
    //         $data = $this->model->reports_date();
    //     } elseif ($_SESSION['profile'] == "1") {
    //         $data = $this->model->reports_dateAdm($company);
    //     }
    //     print $data;
    // }

    // function location()
    // {
    //     if (!isset($_SESSION)) {
    //         session_start();
    //     }
    //     $company = $_SESSION['company'];
    //     if ($_SESSION['profile'] == "4") {
    //         $data = $this->model->locate();
    //     } elseif ($_SESSION['profile'] == "1") {
    //         $data = $this->model->locateAdm($company);
    //     }
    //     print $data;
    // }

    // function age()
    // {
    //     if (!isset($_SESSION)) {
    //         session_start();
    //     }
    //     $company = $_SESSION['company'];
    //     if ($_SESSION['profile'] == "4") {
    //         $data = $this->model->age_range();
    //     } elseif ($_SESSION['profile'] == "1") {
    //         $data = $this->model->age_rangeAdm($company);
    //     }
    //     print $data;
    // }

    // function cases()
    // {
    //     if (!isset($_SESSION)) {
    //         session_start();
    //     }
    //     $company = $_SESSION['company'];
    //     if ($_SESSION['profile'] == "4") {
    //         $data = $this->model->cases_cov();
    //     } elseif ($_SESSION['profile'] == "1") {
    //         $data = $this->model->cases_covAdm($company);
    //     }
    //     print $data;
    // }

    // function transport()
    // {
    //     if (!isset($_SESSION)) {
    //         session_start();
    //     }
    //     $company = $_SESSION['company'];
    //     if ($_SESSION['profile'] == "4") {

    //         $data = $this->model->list_transport();
    //     } elseif ($_SESSION['profile'] == "1") {
    //         $data = $this->model->list_transportAdm($company);
    //     }
    //     print $data;
    // }
}
