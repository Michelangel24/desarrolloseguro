<?php

require_once 'libs/controller.php';
require 'vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

class User extends Controller
{

    function __construct()
    {
        parent::__construct();
        $this->view->mensaje = "";
    }

    function render()
    {
        $this->view->users = $this->model->list();
        $this->view->render('user/index');
    }

    function create()
    {
        $this->view->processes  = $this->model->listProcess();
        $this->view->roles      = $this->model->listRoles();
        $this->view->companies  = $this->model->listCompanies();
        $this->view->render('user/add');
    }

    function save()
    {
        $iduser     = $_POST['iduser'];
        $process    = $_POST['process'];
        $name       = mb_strtoupper($_POST['name'], 'utf-8');
        $surname    = mb_strtoupper($_POST['name'], 'utf-8');
        $phone      = $_POST['phone'];
        $email      = mb_strtolower($_POST['email'], 'utf-8');
        $rol        = $_POST['rol'];
        $company    = $_POST['company'];

        $password   = $this->random_password();

        $passenc    = password_hash($password, PASSWORD_DEFAULT);

        $mensaje = "";

        if ($this->model->save([

            'iduser'    => $iduser,
            'process'   => $process,
            'name'      => $name,
            'surname'   => $surname,
            'phone'     => $phone,
            'email'     => $email,
            'rol'       => $rol,
            'company'   => $company
        ])) {
            if ($this->model->createLogin([
                'user_iduser'   => $iduser,
                'password'      => $passenc
            ])) {
                $this->sendMails($email, $name, $iduser, $password);
            };

            $this->view->mensaje = '
            <div class="alert alert-secondary alert-dismissible fade show" role="alert">
            Usuario almacenado con exito
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            ';
            $this->render();
        } else {
            $this->view->mensaje = '
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
            Error al almacenar la informacion
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            ';
            $this->view->render('user/add');
        }
    }

    function edit($param = null)
    {
        $idempleado = $param[0];
        $user = $this->model->getById($idempleado);

        $this->view->users      = $user;
        $this->view->processes  = $this->model->listProcess();
        $this->view->roles      = $this->model->listRoles();
        $this->view->companies  = $this->model->listCompanies();
        $this->view->mensaje    = "";
        $this->view->render('user/edit');
    }

    function update()
    {
        $iduser     = $_POST['iduser'];
        $process    = $_POST['process'];
        $name       = mb_strtoupper($_POST['name'], 'utf-8');
        $surname    = mb_strtoupper($_POST['name'], 'utf-8');
        $phone      = $_POST['phone'];
        $email      = mb_strtolower($_POST['email'], 'utf-8');
        $rol        = $_POST['rol'];
        $company    = $_POST['company'];


        if ($this->model->update([
            'iduser'    => $iduser,
            'process'   => $process,
            'name'      => $name,
            'surname'   => $surname,
            'phone'     => $phone,
            'email'     => $email,
            'rol'       => $rol,
            'company'   => $company
        ])) {
            $mensaje =
                '<div class="alert alert-primary" role="alert">
                    Usuario actualizado con exito
                </div>';
        } else {
            $mensaje =
                '<div class="alert alert-danger" role="alert">
                    Error al actualizar el usuario
                </div>';
        }
        $this->view->mensaje = $mensaje;
        $this->render();
    }

    function delete($param = null)
    {
        $idempleado = $param[0];
        $this->model->delete($idempleado);
        $mensaje =
            '<div class="alert alert-danger" role="alert">
                    Usuario eliminado con exito
                </div>';
        $this->view->mensaje = $mensaje;
        $this->render();
    }

    function restorePass($param = null)
    {
        $idempleado = $param[0];
        $passenc    = password_hash($idempleado, PASSWORD_DEFAULT);

        $this->model->restore([
            'iduser'    => $idempleado,
            'password'  => $passenc,
        ]);
        $mensaje =
            '<div class="alert alert-success" role="alert">
                    Contraseña reestablecida con exito
                </div>';
        $this->view->mensaje = $mensaje;
        $this->render();
    }

    function searchById()
    {
        if (!isset($_SESSION)) {
            session_start();
        }

        $id = $_POST['idusers'];

        if ($id != "") {

            if ($users = $this->model->search($id)) {
                $mensaje = '';
                $this->view->mensaje = $mensaje;
                $this->view->users = $users;
                $this->view->render('users/index');
            } else {
                $mensaje = '<div class="alert alert-danger" role="alert">
                    No se encontro ningun registro con el criterio de busqueda seleccionado
                </div>';
                $this->view->mensaje = $mensaje;
                $this->render();
            }
        } else {
            $mensaje = '<div class="alert alert-danger" role="alert">
            Debe ingresar un dato
            </div>';
            $this->view->mensaje = $mensaje;
            $this->render();
        }
    }

    function sendMails($email, $name, $iduser, $password)
    {
        // print_r($items);
        // echo !extension_loaded('openssl')?"Not Available":"Available";
        //Crear una instancia de PHPMailer
        $mail = new PHPMailer();
        //Definir que vamos a usar SMTP
        $mail->IsSMTP();
        //Esto es para activar el modo depuración. En entorno de pruebas lo mejor es 2, en producción siempre 0
        // 0 = off (producción)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug  = 0;
        //Ahora definimos gmail como servidor que aloja nuestro SMTP
        $mail->Host       = 'smtp.gmail.com';
        //El puerto será el 587 ya que usamos encriptación TLS
        $mail->Port       = 587;
        //Definmos la seguridad como TLS
        $mail->SMTPSecure = 'tls';
        //Tenemos que usar gmail autenticados, así que esto a TRUE
        $mail->SMTPAuth   = true;

        $mail->SMTPAutoTLS = false;
        //Definimos la cuenta que vamos a usar. Dirección completa de la misma
        $mail->Username   = "soporte@si18.com.co";
        //Introducimos nuestra contraseña de gmail
        $mail->Password   = "*S1Tec99*";
        //Definimos el remitente (dirección y, opcionalmente, nombre)
        $mail->SetFrom('soporte@si18.com.co', 'App Lecciones Aprendidas');
        //Esta línea es por si queréis enviar copia a alguien (dirección y, opcionalmente, nombre)
        // $mail->AddReplyTo('replyto@correoquesea.com','El de la réplica');
        //Y, ahora sí, definimos el destinatario (dirección y, opcionalmente, nombre)
        $mail->AddAddress($email, $name);
        //Definimos el tema del email
        $mail->Subject = 'Acceso App Lecciones Aprendidas';
        $mail->CharSet = 'UTF-8';

        $mail->Body =
            'Buen día Señor ' . $name . ','  . "\n" . "\n" .
            'Para facilitar el proceso de acceso a la plataforma Lecciones Aprendidas, sus datos de acceso son:' . "\n" .
            'Usuario:    ' . $iduser . "\n" .
            'Contraseña: ' . $password  . "\n";
        //Para enviar un correo formateado en HTML lo cargamos con la siguiente función. Si no, puedes meterle directamente una cadena de texto.
        // $mail->MsgHTML(file_get_contents('correomaquetado.html'), dirname(ruta_al_archivo));
        //Y por si nos bloquean el contenido HTML (algunos correos lo hacen por seguridad) una versión alternativa en texto plano (también será válida para lectores de pantalla)
        // $mail->AltBody = 'This is a plain-text message body';
        //Enviamos el correo

        // echo $user->email;
        // print_r($mail);
        if (!$mail->Send()) {
            // echo "Error: " . $mail->ErrorInfo;
            // return false;
        } else {
            // echo "Enviado!";
            // return true;
        }
    }

    function random_password()
    {
        $longitud = 8; // longitud del password  
        $pass = substr(md5(rand()), 0, $longitud);
        return $pass; // devuelve el password   
    }
}
