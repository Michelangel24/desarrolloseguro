<?php

class LoginModel extends Model{

    public function __construct(){
        parent::__construct();
    }

    public function login($data){
        $item = new Login();

        try{
            $query = $this->db->connect()->prepare('
            SELECT
                `login`.`user_iduser`,
                `login`.`password`,
                `login`.`token`,
                `user`.`name`,
                `user`.`surname`,
                `user`.`email`,
                `user`.`rol_idrol`,
                `user`.`company_idcompany`,
                `user`.`process_idprocess`
            FROM
                login
            INNER JOIN USER ON login.user_iduser = USER.iduser
            WHERE
                login.user_iduser = :iduser
            ');
            $query->execute([
                'iduser' => $data['iduser']
            ]);

            while ($row = $query->fetch()) {
                $item->iduser               = $row['user_iduser'];
                $item->password             = $row['password'];
                $item->name                 = $row['name'];
                $item->email                = $row['email'];
                $item->token                = $row['token'];
                $item->rol_idrol            = $row['rol_idrol'];
                $item->company_idcompany    = $row['company_idcompany'];
                $item->process_idprocess    = $row['process_idprocess'];
            }

            return $item;
        }catch(PDOException $e){
            // echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return null;
        }
    }

    public function createToken($data)
    {

        try {
            $query = $this->db->connect()->prepare('
            UPDATE
                `login`
            SET
                `token` = :token
            WHERE
                `user_iduser` = :user_iduser
            ');
            $query->execute([

                'user_iduser'   => $data['user_iduser'],
                'token'         => $data['token']
            ]);
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return false;
        }
    }

}
