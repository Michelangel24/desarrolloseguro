<?php


require_once 'models/users.php';
require_once 'models/companies.php';
require_once 'models/processes.php';

class IndexModel extends Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function list()
    {
        $items = [];

        try {
            $query = $this->db->connect()->prepare('SELECT * FROM users');
            $query->execute([]);

            while ($row = $query->fetch()) {
                $item = new Users();

                $item->idusers   = $row['idusers'];
                $item->name      = $row['name'];
                $item->quantity  = $row['quantity'];

                array_push($items, $item);
            }

            return $items;
        } catch (PDOException $e) {
            echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return [];
        }
    }

    public function getById($id)
    {
        $item = new Users();

        $query = $this->db->connect()->prepare("SELECT * FROM users WHERE idusers = :idusers");

        try {
            $query->execute(['idusers' => $id]);

            while ($row = $query->fetch()) {

                $item->idusers   = $row['idusers'];
                $item->name      = $row['name'];
                $item->quantity  = $row['quantity'];
            }

            return $item;
        } catch (PDOException $e) {
            return null;
        }
    }

    public function save($data)
    {

        try {
            $query = $this->db->connect()->prepare('UPDATE users SET quantity = :quantity WHERE idusers = :idusers');
            $query->execute([
                'idusers'   => $data['idusers'],
                'quantity'  => $data['quantity'],
            ]);

            return true;
        } catch (PDOException $e) {
            // echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return false;
        }
    }

    public function listCompany()
    {
        $items = [];

        try {
            $query = $this->db->connect()->prepare('SELECT * FROM company');
            $query->execute([]);

            while ($row = $query->fetch()) {
                $item = new Companies();

                $item->idcompany    = $row['idcompany'];
                $item->description  = $row['description'];
                $item->comment      = $row['comment'];

                array_push($items, $item);
            }

            return $items;
        } catch (PDOException $e) {
            // echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return [];
        }
    }

    public function listProcess()
    {
        $items = [];

        try {
            $query = $this->db->connect()->prepare('SELECT * FROM process');
            $query->execute([]);

            while ($row = $query->fetch()) {
                $item = new Processes();

                $item->idprocess    = $row['idprocess'];
                $item->description  = $row['description'];
                $item->comment      = $row['comment'];

                array_push($items, $item);
            }

            return $items;
        } catch (PDOException $e) {
            // echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return [];
        }
    }
}
