<?php


require_once 'models/users.php';
require_once 'models/roles.php';
require_once 'models/companies.php';
require_once 'models/processes.php';


class UserModel extends Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function save($data)
    {

        try {
            $query = $this->db->connect()->prepare('
            INSERT INTO `user`(
                `iduser`,
                `process_idprocess`,
                `name`,
                `surname`,
                `phone`,
                `email`,
                `rol_idrol`,
                `company_idcompany`
            )
            VALUES(
                :iduser,
                :process_idprocess,
                :name,
                :surname,
                :phone,
                :email,
                :rol_idrol,
                :company_idcompany
            )
            ');
            $query->execute([
                'iduser'            => $data['iduser'],
                'process_idprocess' => $data['process'],
                'name'              => $data['name'],
                'surname'           => $data['surname'],
                'phone'             => $data['phone'],
                'email'             => $data['email'],
                'rol_idrol'         => $data['rol'],
                'company_idcompany' => $data['company']
            ]);

            return true;
        } catch (PDOException $e) {
            // echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return false;
        }
    }

    public function createLogin($data)
    {

        try {
            $query = $this->db->connect()->prepare('INSERT INTO login (USER_IDUSER, PASSWORD) VALUES (:user_iduser, :password)');
            $query->execute([

                'user_iduser'   => $data['user_iduser'],
                'password'      => $data['password']
            ]);
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return false;
        }
    }

    public function listRoles()
    {
        $items = [];

        try {
            $query = $this->db->connect()->query("SELECT idrol, description, comment FROM rol");

            while ($row = $query->fetch()) {
                $item = new Roles();

                $item->idrol        = $row['idrol'];
                $item->description  = $row['description'];
                $item->comment      = $row['comment'];

                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return false;
        }
    }

    public function list()
    {
        $items = [];

        try {
            $query = $this->db->connect()->query("
            SELECT user.iduser, process.description AS process, user.name, user.surname, user.phone, user.email, rol.description AS rol
            FROM user
            INNER JOIN rol ON user.rol_idrol = rol.idrol
            INNER JOIN process ON user.process_idprocess = process.idprocess
            ORDER BY user.iduser ASC;
            ");

            while ($row = $query->fetch()) {
                $item = new Users();

                $item->iduser               = $row['iduser'];
                $item->process_idprocess    = $row['process'];
                $item->name                 = $row['name'];
                $item->surname              = $row['surname'];
                $item->phone                = $row['phone'];
                $item->email                = $row['email'];
                $item->rol_idrol            = $row['rol'];

                array_push($items, $item);
            }
            return $items;
        } catch (PDOException $e) {
            echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return false;
        }
    }

    public function listCompanies()
    {
        $items = [];

        try {
            $query = $this->db->connect()->prepare('SELECT * FROM company');
            $query->execute([]);

            while ($row = $query->fetch()) {
                $item = new Companies();

                $item->idcompany    = $row['idcompany'];
                $item->description  = $row['description'];
                $item->comment      = $row['comment'];

                array_push($items, $item);
            }

            return $items;
        } catch (PDOException $e) {
            echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return [];
        }
    }

    public function getById($id)
    {
        $item = new User();

        $query = $this->db->connect()->prepare("SELECT * FROM user WHERE iduser = :iduser");

        try {
            $query->execute(['iduser' => $id]);

            while ($row = $query->fetch()) {
                $item->iduser               = $row['iduser'];
                $item->process_idprocess    = $row['process_idprocess'];
                $item->name                 = $row['name'];
                $item->surname              = $row['surname'];
                $item->phone                = $row['phone'];
                $item->email                = $row['email'];
                $item->rol_idrol            = $row['rol_idrol'];
                $item->company_idcompany    = $row['company_idcompany'];

            }

            return $item;
        } catch (PDOException $e) {
            return null;
        }
    }

    public function update($data)
    {
        $query = $this->db->connect()->prepare("
        UPDATE
            `user`
        SET
            `process_idprocess`     = :process_idprocess, 
            `name`                  = :name, 
            `surname`               = :surname, 
            `phone`                 = :phone, 
            `email`                 = :email, 
            `rol_idrol`             = :rol_idrol, 
            `company_idcompany`     = :company_idcompany
        WHERE 
            `iduser`                = :iduser
            ");
        try {
            $query->execute([
                'iduser'            => $data['iduser'],
                'process_idprocess' => $data['process'],
                'name'              => $data['name'],
                'surname'           => $data['surname'],
                'phone'             => $data['phone'],
                'email'             => $data['email'],
                'rol_idrol'         => $data['rol'],
                'company_idcompany' => $data['company']

            ]);

            return true;
        } catch (PDOException $e) {
            // echo ("entro aqui");
            // echo $e->getMessage();
            // print_r($e);
            // echo "Este documento ya esta registrado";
            return false;
        }
    }

    public function delete($id)
    {

        $query = $this->db->connect()->prepare("DELETE FROM user WHERE iduser = :iduser");

        try {
            $query->execute(['iduser' => $id]);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    public function restore($data)
    {

        try {
            $query = $this->db->connect()->prepare('UPDATE login SET password = :password WHERE user_iduser = :user_iduser');
            $query->execute([

                'user_iduser'   => $data['iduser'],
                'password'      => $data['password']
            ]);
            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return false;
        }
    }

    public function search($id)
    {
        $items = [];

        $query = $this->db->connect()->prepare("
        SELECT * FROM user INNER JOIN roles ON user.rol = roles.idrol
         WHERE iduser = :id
         ");

        try {
            $query->execute(['id' => $id]);

            while ($row = $query->fetch()) {
                $item = new Users();

                $item->iduser      = $row['iduser'];
                $item->name         = $row['name'];
                $item->rol          = $row['description'];
                $item->email        = $row['email'];

                array_push($items, $item);
            }

            return $items;
        } catch (PDOException $e) {
            return null;
        }
    }

    public function listProcess()
    {
        $items = [];

        try {
            $query = $this->db->connect()->prepare('SELECT * FROM process');
            $query->execute([]);

            while ($row = $query->fetch()) {
                $item = new Processes();

                $item->idprocess    = $row['idprocess'];
                $item->description  = $row['description'];
                $item->comment      = $row['comment'];

                array_push($items, $item);
            }

            return $items;
        } catch (PDOException $e) {
            // echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return [];
        }
    }

}
