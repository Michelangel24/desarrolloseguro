<?php

class Lessons{

    public $idlearned_lesson;
    public $user_iduser;
    public $date_insert;
    public $company_idcompany;
    public $desc_company;
    public $situation;
    public $cause;
    public $lesson;
    public $practice;
    public $diffusion;
    public $process_idprocess;
    public $desc_process;
    public $lesson_type_idlesson_type;
    public $desc_lesson_type;
    public $document;
    public $attached_file;
}
