<?php


require_once 'models/companies.php';
require_once 'models/processes.php';
require_once 'models/lessons.php';
require_once 'models/lessontypes.php';


class LessonModel extends Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function list()
    {
        $items = [];

        try {
            $query = $this->db->connect()->prepare('SELECT * FROM users');
            $query->execute([]);

            while ($row = $query->fetch()) {
                $item = new Users();

                $item->idusers   = $row['idusers'];
                $item->name      = $row['name'];
                $item->quantity  = $row['quantity'];

                array_push($items, $item);
            }

            return $items;
        } catch (PDOException $e) {
            echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return [];
        }
    }

    public function getById($id)
    {
        $item = new Users();

        $query = $this->db->connect()->prepare("SELECT * FROM users WHERE idusers = :idusers");

        try {
            $query->execute(['idusers' => $id]);

            while ($row = $query->fetch()) {

                $item->idusers   = $row['idusers'];
                $item->name      = $row['name'];
                $item->quantity  = $row['quantity'];
            }

            return $item;
        } catch (PDOException $e) {
            return null;
        }
    }

    public function save($data)
    {
        // print_r($data);
        try {
            $query = $this->db->connect()->prepare('
            INSERT INTO 
            lesson_learned 
            (user_iduser, company_idcompany, process_idprocess, situation, cause, lesson, practice, diffusion, lesson_type_idlesson_type, document, attached_file) 
            VALUES 
            (:user_iduser, :company_idcompany, :process_idprocess, :situation, :cause, :lesson, :practice, :diffusion, :lesson_type_idlesson_type, :document, :attached_file)
            ');
            $query->execute([
                'user_iduser'               => $data['user_iduser'],
                'company_idcompany'         => $data['company_idcompany'],
                'process_idprocess'         => $data['process_idprocess'],
                'situation'                 => $data['situation'],
                'cause'                     => $data['cause'],
                'lesson'                    => $data['lesson'],
                'practice'                  => $data['practice'],
                'diffusion'                 => $data['diffusion'],
                'lesson_type_idlesson_type' => $data['lesson_type_idlesson_type'],
                'document'                  => $data['document'],
                'attached_file'             => $data['attached_file']
            ]);

            return true;
        } catch (PDOException $e) {
            echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return false;
        }
    }

    public function listCompany()
    {
        $items = [];

        try {
            $query = $this->db->connect()->prepare('SELECT * FROM company');
            $query->execute([]);

            while ($row = $query->fetch()) {
                $item = new Companies();

                $item->idcompany    = $row['idcompany'];
                $item->description  = $row['description'];
                $item->comment      = $row['comment'];

                array_push($items, $item);
            }

            return $items;
        } catch (PDOException $e) {
            // echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return [];
        }
    }

    public function listProcess()
    {
        $items = [];

        try {
            $query = $this->db->connect()->prepare('SELECT * FROM process');
            $query->execute([]);

            while ($row = $query->fetch()) {
                $item = new Processes();

                $item->idprocess    = $row['idprocess'];
                $item->description  = $row['description'];
                $item->comment      = $row['comment'];

                array_push($items, $item);
            }

            return $items;
        } catch (PDOException $e) {
            // echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return [];
        }
    }

    public function listLessonType()
    {
        $items = [];

        try {
            $query = $this->db->connect()->prepare('SELECT * FROM lesson_type');
            $query->execute([]);

            while ($row = $query->fetch()) {
                $item = new LessonTypes();

                $item->idlesson_type    = $row['idlesson_type'];
                $item->description      = $row['description'];
                $item->comment          = $row['comment'];

                array_push($items, $item);
            }

            return $items;
        } catch (PDOException $e) {
            // echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return [];
        }
    }

    public function listLesson()
    {
        $items = [];

        try {
            $query = $this->db->connect()->prepare('
            SELECT
                `idlearned_lesson`,
                `user_iduser`,
                `company_idcompany`,
                `situation`,
                `cause`,
                `lesson`,
                `date_insert`,
                `practice`,
                `diffusion`,
                `process_idprocess`,
                `lesson_type_idlesson_type`,
                `document`,
                `attached_file`,
                `company`.`description` AS `desc_company`,
                `process`.`description` AS `desc_process`,
                `lesson_type`.`description` AS `desc_lesson_type`
            FROM
                `lesson_learned`
            INNER JOIN company ON `company_idcompany` = `idcompany`
            INNER JOIN process ON `process_idprocess` = `idprocess`
            INNER JOIN lesson_type ON `lesson_type_idlesson_type` = `idlesson_type`
            ');
            $query->execute([]);

            while ($row = $query->fetch()) {
                $item = new Lessons();

                $item->idlearned_lesson             = $row['idlearned_lesson'];
                $item->user_iduser                  = $row['user_iduser'];
                $item->company_idcompany            = $row['company_idcompany'];
                $item->desc_company                 = $row['desc_company'];
                $item->situation                    = $row['situation'];
                $item->cause                        = $row['cause'];
                $item->lesson                       = $row['lesson'];
                $item->date_insert                  = $row['date_insert'];
                $item->practice                     = $row['practice'];
                $item->process_idprocess            = $row['process_idprocess'];
                $item->desc_process                 = $row['desc_process'];
                $item->lesson_type_idlesson_type    = $row['lesson_type_idlesson_type'];
                $item->desc_lesson_type             = $row['desc_lesson_type'];
                $item->document                     = $row['document'];
                $item->attached_file                = $row['attached_file'];

                array_push($items, $item);
            }

            return $items;
        } catch (PDOException $e) {
            // echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return [];
        }
    }

    public function getLessonById($idlesson)
    {
        $items = [];

        try {
            $query = $this->db->connect()->prepare('
            SELECT
                `idlearned_lesson`,
                `user_iduser`,
                `company_idcompany`,
                `situation`,
                `cause`,
                `lesson`,
                `date_insert`,
                `practice`,
                `diffusion`,
                `process_idprocess`,
                `lesson_type_idlesson_type`,
                `document`,
                `attached_file`,
                `company`.`description` AS `desc_company`,
                `process`.`description` AS `desc_process`,
                `lesson_type`.`description` AS `desc_lesson_type`
            FROM
                `lesson_learned`
            INNER JOIN company ON `company_idcompany` = `idcompany`
            INNER JOIN process ON `process_idprocess` = `idprocess`
            INNER JOIN lesson_type ON `lesson_type_idlesson_type` = `idlesson_type`
            WHERE
                `idlearned_lesson` = :idlesson
            ');
            $query->execute(['idlesson' => $idlesson]);

            while ($row = $query->fetch()) {
                $item = new Lessons();

                $item->idlearned_lesson             = $row['idlearned_lesson'];
                $item->user_iduser                  = $row['user_iduser'];
                $item->company_idcompany            = $row['company_idcompany'];
                $item->desc_company                 = $row['desc_company'];
                $item->situation                    = $row['situation'];
                $item->cause                        = $row['cause'];
                $item->lesson                       = $row['lesson'];
                $item->date_insert                  = $row['date_insert'];
                $item->practice                     = $row['practice'];
                $item->diffusion                    = $row['diffusion'];
                $item->process_idprocess            = $row['process_idprocess'];
                $item->desc_process                 = $row['desc_process'];
                $item->lesson_type_idlesson_type    = $row['lesson_type_idlesson_type'];
                $item->desc_lesson_type             = $row['desc_lesson_type'];
                $item->document                     = $row['document'];
                $item->attached_file                = $row['attached_file'];

                array_push($items, $item);
            }

            return $items;
        } catch (PDOException $e) {
            echo $e->getMessage();
            // echo "Este documento ya esta registrado";
            return [];
        }
    }
}
