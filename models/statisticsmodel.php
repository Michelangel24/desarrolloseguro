<?php

class StatisticsModel extends Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function list_company()
    {
        try {
            $query = $this->db->connect()->prepare('
            SELECT
                company.`description`,
                COUNT(*) AS users
            FROM
                lesson_learned
            INNER JOIN company ON lesson_learned.`company_idcompany` = company.`idcompany`
            GROUP BY
                company.`idcompany`
            ');

            $query->execute([]);
            $data = array();
            foreach ($query as $row) {
                $data[] = $row;
            }
            return json_encode($data);
        } catch (PDOException $e) {
            // echo "Este documento ya esta registrado";
            return false;
        }
    }

    public function list_process()
    {
        try {
            $query = $this->db->connect()->prepare('
            SELECT
                process.`description`,
                COUNT(*) AS users
            FROM
                lesson_learned
            INNER JOIN process ON lesson_learned.`process_idprocess` = process.`idprocess`
            GROUP BY
                process.`idprocess`
            ');

            $query->execute([]);
            $data = array();
            foreach ($query as $row) {
                $data[] = $row;
            }
            return json_encode($data);
        } catch (PDOException $e) {
            // echo "Este documento ya esta registrado";
            return false;
        }
    }
    
    public function list_lessonType()
    {
        try {
            $query = $this->db->connect()->prepare('
            SELECT
                lesson_type.`description`,
                COUNT(*) AS users
            FROM
                lesson_learned
            INNER JOIN lesson_type ON lesson_learned.`lesson_type_idlesson_type` = lesson_type.`idlesson_type`
            GROUP BY
                lesson_type.`idlesson_type`
            ');

            $query->execute([]);
            $data = array();
            foreach ($query as $row) {
                $data[] = $row;
            }
            return json_encode($data);
        } catch (PDOException $e) {
            // echo "Este documento ya esta registrado";
            return false;
        }
    }
    
    public function list_dateInsert()
    {
        try {
            $query = $this->db->connect()->prepare('
            SELECT
                MONTH(`date_insert`) Mes,
                COUNT(*) AS users
            FROM
                lesson_learned
            GROUP BY
                Mes
            ');

            $query->execute([]);
            $data = array();
            foreach ($query as $row) {
                $data[] = $row;
            }
            return json_encode($data);
        } catch (PDOException $e) {
            // echo "Este documento ya esta registrado";
            return false;
        }
    }


//     public function totalUsers()
//     {
//         try {
//             $query = $this->db->connect()->query(
//                 'select count(*) as totalusers
//                 from users 
//                 WHERE `idusers` != 123456'
//             );
//             $quantity = $query->fetchColumn();
//             return $quantity;
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }

//     public function totalUsersAdm($company)
//     {
//         try {
//             $query = $this->db->connect()->prepare(
//                 'select count(*) as totalusers
//                 from users 
//                 WHERE `idusers` != 123456 AND company = :company'
//             );
//             $query->execute(['company' => $company]);
//             $quantity = $query->fetchColumn();
//             return $quantity;
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }

//     public function enfermedades()
//     {
//         try {
//             $query = $this->db->connect()->prepare(
//                 'select usersdiseasesrelation.`iddiseases`, diseases.`description` ,
//                 count(*) as users
//                 from usersdiseasesrelation
//                 INNER JOIN diseases ON usersdiseasesrelation.`iddiseases` = diseases.`iddiseases`
//                 INNER JOIN users ON usersdiseasesrelation.`idusers` = users.`idusers`
//                 WHERE users.`idusers` != 123456
//                 group by usersdiseasesrelation.`iddiseases`'
//             );
//             $query->execute([]);
//             $data = array();
//             foreach ($query as $row) {
//                 $data[] = $row;
//             }
//             return json_encode($data);
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }

//     public function enfermedadesAdm($company)
//     {
//         try {
//             $query = $this->db->connect()->prepare(
//                 'select usersdiseasesrelation.`iddiseases`, diseases.`description` ,
//                 count(*) as users
//                 from usersdiseasesrelation
//                 INNER JOIN diseases ON usersdiseasesrelation.`iddiseases` = diseases.`iddiseases`
//                 INNER JOIN users ON usersdiseasesrelation.`idusers` = users.`idusers`
//                 WHERE company = :company AND users.`idusers` != 123456
//                 group by usersdiseasesrelation.`iddiseases`'
//             );
//             $query->execute(['company' => $company]);
//             $data = array();
//             foreach ($query as $row) {
//                 $data[] = $row;
//             }
//             return json_encode($data);
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }

//     public function reports_date()
//     {
//         try {
//             $query = $this->db->connect()->prepare(
//                 'select `date_report` , count(*) as users 
//                 from reports WHERE `date_report` BETWEEN NOW() - INTERVAL 7 DAY AND NOW() AND `users_idusers` != 123456
//                 group by `date_report`'
//             );
//             $query->execute([]);
//             $data = array();
//             foreach ($query as $row) {
//                 $data[] = $row;
//             }
//             return json_encode($data);
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }
//     public function reports_dateAdm($company)
//     {
//         try {
//             $query = $this->db->connect()->prepare(
//                 'select `date_report` , count(*) as users 
//                 from reports 
//                 INNER JOIN users ON reports.`users_idusers` = users.`idusers`
//                 WHERE `date_report` BETWEEN NOW() - INTERVAL 7 DAY AND NOW() AND company = :company AND `users_idusers` != 123456
//                 group by `date_report`'
//             );
//             $query->execute(['company' => $company]);
//             $data = array();
//             foreach ($query as $row) {
//                 $data[] = $row;
//             }
//             return json_encode($data);
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }

//     public function locate()
//     {
//         try {
//             $query = $this->db->connect()->prepare(
//                 'select `location` , count(*) as users from users
//                 WHERE `idusers` != 123456
//                 group by `location`'
//             );
//             $query->execute([]);
//             $data = array();
//             foreach ($query as $row) {
//                 $data[] = $row;
//             }
//             return json_encode($data);
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }
//     public function locateAdm($company)
//     {
//         try {
//             $query = $this->db->connect()->prepare(
//                 'select `location` , count(*) as users 
//                 from users 
//                 where `company` = :company AND `idusers` != 123456
//                 group by `location`'
//             );
//             $query->execute(['company' => $company]);
//             $data = array();
//             foreach ($query as $row) {
//                 $data[] = $row;
//             }
//             return json_encode($data);
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }

//     public function age_range()
//     {
//         try {
//             $query = $this->db->connect()->prepare(
//                 'select count(*) as users, 
//                 case 
//                 when (YEAR(CURDATE())-YEAR(`born_date`) +
//                 IF(DATE_FORMAT(CURDATE(),"%m-%d") > DATE_FORMAT(`born_date`,"%m-%d"), 0 , -1 )) between 0 and 15 then "0 a 15" 
//                 when (YEAR(CURDATE())-YEAR(`born_date`) +
//                 IF(DATE_FORMAT(CURDATE(),"%m-%d") > DATE_FORMAT(`born_date`,"%m-%d"), 0 , -1 )) between 16 and 30 then "16 a 30" 
//                 when (YEAR(CURDATE())-YEAR(`born_date`) +
//                 IF(DATE_FORMAT(CURDATE(),"%m-%d") > DATE_FORMAT(`born_date`,"%m-%d"), 0 , -1 )) between 31 and 45 then "31 a 45" 
//                 when (YEAR(CURDATE())-YEAR(`born_date`) +
//                 IF(DATE_FORMAT(CURDATE(),"%m-%d") > DATE_FORMAT(`born_date`,"%m-%d"), 0 , -1 )) between 46 and 60 then "46 a 60" 
//                 ELSE "mas de 60" end as rango_anios
//                 from users 
//                 WHERE `idusers` != 123456
//                 group by rango_anios'
//             );

//             $query->execute([]);
//             $data = array();
//             foreach ($query as $row) {
//                 $data[] = $row;
//             }
//             return json_encode($data);
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }
//     public function age_rangeAdm($company)
//     {
//         try {
//             $query = $this->db->connect()->prepare(
//                 'select count(*) as users, 
//                 case 
//                 when (YEAR(CURDATE())-YEAR(`born_date`) +
//                 IF(DATE_FORMAT(CURDATE(),"%m-%d") > DATE_FORMAT(`born_date`,"%m-%d"), 0 , -1 )) between 0 and 15 then "0 a 15" 
//                 when (YEAR(CURDATE())-YEAR(`born_date`) +
//                 IF(DATE_FORMAT(CURDATE(),"%m-%d") > DATE_FORMAT(`born_date`,"%m-%d"), 0 , -1 )) between 16 and 30 then "16 a 30" 
//                 when (YEAR(CURDATE())-YEAR(`born_date`) +
//                 IF(DATE_FORMAT(CURDATE(),"%m-%d") > DATE_FORMAT(`born_date`,"%m-%d"), 0 , -1 )) between 31 and 45 then "31 a 45" 
//                 when (YEAR(CURDATE())-YEAR(`born_date`) +
//                 IF(DATE_FORMAT(CURDATE(),"%m-%d") > DATE_FORMAT(`born_date`,"%m-%d"), 0 , -1 )) between 46 and 60 then "46 a 60" 
//                 ELSE "mas de 60" end as rango_anios 
//                 from users 
//                 WHERE company = :company AND `idusers` != 123456
//                 group by rango_anios'
//             );

//             $query->execute(['company' => $company]);
//             $data = array();
//             foreach ($query as $row) {
//                 $data[] = $row;
//             }
//             return json_encode($data);
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }

//     public function cases_cov()
//     {
//         try {
//             $query = $this->db->connect()->prepare(
//                 'select count(*) as users, 
//                 case 
//                 when `cov_case` = 1 then "SANOS" 
//                 when `cov_case` = 2 then "SOSPECHOSOS"
//                 when `cov_case` = 3 then "CONFIRMADOS"
//                 when `cov_case` = 4 then "RECUPERADOS"
//                 end as casos 
//                 from users 
//                 WHERE `idusers` != 123456
//                 group by casos
//                 ORDER BY `cov_case`'
//             );

//             $query->execute([]);
//             $data = array();
//             foreach ($query as $row) {
//                 $data[] = $row;
//             }
//             return json_encode($data);
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }
//     public function cases_covAdm($company)
//     {
//         try {
//             $query = $this->db->connect()->prepare(
//                 'select count(*) as users, 
//                 case 
//                 when `cov_case` = 1 then "SANOS" 
//                 when `cov_case` = 2 then "SOSPECHOSOS"
//                 when `cov_case` = 3 then "CONFIRMADOS"
//                 when `cov_case` = 4 then "RECUPERADOS"
//                 end as casos 
//                 from users 
//                 WHERE company = :company AND `idusers` != 123456
//                 group by casos
//                 ORDER BY `cov_case`'
//             );

//             $query->execute(['company' => $company]);
//             $data = array();
//             foreach ($query as $row) {
//                 $data[] = $row;
//             }
//             return json_encode($data);
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }

    
//     public function list_transport()
//     {
//         try {
//             $query = $this->db->connect()->prepare(
//                 'select 
//                 users_transport_relation.`idtransport`, transport.`description` ,
//                  count(*) as users from users_transport_relation 
//                  INNER JOIN transport ON users_transport_relation.`idtransport` = transport.`idtransport` 
//                  INNER JOIN users ON users_transport_relation.`idusers` = users.`idusers`
//                  WHERE users.`idusers` != 123456
//                  group by users_transport_relation.`idtransport`'
//             );

//             $query->execute([]);
//             $data = array();
//             foreach ($query as $row) {
//                 $data[] = $row;
//             }
//             return json_encode($data);
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }

//     public function list_transportAdm($company)
//     {
//         try {
//             $query = $this->db->connect()->prepare(
//                 'select 
//                 users_transport_relation.`idtransport`, transport.`description` ,
//                  count(*) as users from users_transport_relation 
//                  INNER JOIN transport ON users_transport_relation.`idtransport` = transport.`idtransport` 
//                  INNER JOIN users ON users_transport_relation.`idusers` = users.`idusers`
//                  WHERE users.`company` = :company AND users.`idusers` != 123456
//                  group by users_transport_relation.`idtransport`'
//             );

//             $query->execute(['company' => $company]);
//             $data = array();
//             foreach ($query as $row) {
//                 $data[] = $row;
//             }
//             return json_encode($data);
//         } catch (PDOException $e) {
//             // echo "Este documento ya esta registrado";
//             return false;
//         }
//     }
}
